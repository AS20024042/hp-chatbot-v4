// Import required pckages
const path = require('path');
const express=require('express');
const schema=require('./Mongooseschema');
const mongoose=require('mongoose');
const MongoClient = require('mongodb').MongoClient;
// Import required bot services. See https://aka.ms/bot-services to learn more about the different parts of a bot.
const { BotFrameworkAdapter, MemoryStorage, ConversationState, UserState } = require('botbuilder');
// This bot's main dialog.
const { DialogAndWelcomeBot } = require('./bots/dialogAndWelcomeBot');
const { MainDialog } = require('./dialogs/mainDialog');
const restify=require('restify')
// Note: Ensure you have a .env file and include LuisAppId, LuisAPIKey and LuisAPIHostName.
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({ path: ENV_FILE });
//database.
const uri="mongodb+srv://aryamanpanth:aryaman123@cluster0-3xshn.mongodb.net/test?retryWrites=true&w=majority"
mongoose.connect(uri,{useNewUrlParser:true});
const db=mongoose.connection;
console.log("Connected to database");

/*
client.connect(err => {
  if(err)
  {
      throw err
  }
    const collection = client.db("test").collection("devices");
  
  console.log("connected");
  //var ins={name:"Aryaman panth",email:"aryaman.panth@wipro.com"};
  //collection.insertOne(ins,(err,res)=>{
      
    //  console.log("Data inserted successfully")
  //});

const transcript=schema.transcript;
const addtranscript=new transcript({
    from:{id:'235789964544',
          name:"Bot"},
    to:{id:'1242452345',
        name:'user'   },
    message:"HI this is transcript"
})
const conversation=schema.Conversation;
const newconversation=new conversation({
  message:"Aryaman Panth",
  channelId:"Facebook",
  transcripts:addtranscript


});
collection.insertOne(newconversation,(err,res)=>{
console.log("Data inserted successd")
});

client.close();
});

*/


//middleware

const { HandoverMiddleware, ArrayHandoverProvider } = require('./middleware');

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about adapters.
const adapter = new BotFrameworkAdapter({
    appId: "d139d94d-16db-456a-b3c5-e9de54a47f2c",
    appPassword: "GA][c:nxl6]G%nD&g[qjbZ9.^#60*%"
});

const provider = new ArrayHandoverProvider();
adapter.use(new HandoverMiddleware(provider, adapter));


// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
    // This check writes out errors to console log
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights.
    console.error(`\n [onTurnError]: ${ error }`);
    // Send a message to the user
    await context.sendActivity(`Oops. Something went wrong!`);
    // Clear out state
    await conversationState.delete(context);
};

// Define a state store for your bot. See https://aka.ms/about-bot-state to learn more about using MemoryStorage.
// A bot requires a state store to persist the dialog and user state between messages.
let conversationState, userState;

// For local development, in-memory storage is used.
// CAUTION: The Memory Storage used here is for local bot debugging only. When the bot
// is restarted, anything stored in memory will be gone.
const memoryStorage = new MemoryStorage();
conversationState = new ConversationState(memoryStorage);
userState = new UserState(memoryStorage);



// Create the main dialog.
const dialog = new MainDialog();
const bot = new DialogAndWelcomeBot(conversationState, userState, dialog);
// Create HTTP server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, () => {
    console.log(`\n${ server.name } listening to ${ server.url }`);
    console.log(`\nGet Bot Framework Emulator: https://aka.ms/botframework-emulator`);
    console.log(`\nTo talk to your bot, open handoff.bot file in the Emulator`);
});


// Listen for incoming activities and route them to your bot main dialog.
server.post('/api/messages', (req, res) => 
{
    // Route received a request to adapter for processing
    adapter.processActivity(req, res, async (turnContext) => {
        // route to bot activity handler.
        await bot.run(turnContext);
    });
});

server.get('/*', restify.plugins.serveStatic({
    directory: path.join(__dirname, "public"),
    default: '/useroragent.html'
}));
