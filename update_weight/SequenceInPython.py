import requests
import pandas as pd

def Remove(duplicate):
    final_list = []
    for num in duplicate:
        if num not in final_list:
            final_list.append(num)
    return final_list

def LUIS_IdentifiedSequence():
    fil = pd.ExcelFile('update_weight/sample_chat_Performance_games.xlsx')
    fil1=pd.read_excel(fil,'1395')
    fil2=pd.read_excel(fil,'13467')
    fil3=pd.read_excel(fil,'15611')
    fil4=pd.read_excel(fil,'5139')
    fil5=pd.read_excel(fil,'51342')
    fil6=pd.read_excel(fil,'42675')
    fil7=pd.read_excel(fil,'426111')
    fil8=pd.read_excel(fil,'4125')
    q = [fil1, fil2, fil3, fil4, fil5, fil6, fil7, fil8]
    sol = []
    headers = {'Ocp-Apim-Subscription-Key': '50fb768e0dbb490f99a12f5ab84d7d25'}  # Request headers
    params =({
                        'timezoneOffset': '0',
                        'verbose': 'false',
                        'spellCheck': 'false',
                        'staging': 'false',
                            })
    for k in range(0, 8):
        a = len(q[k])
        out = []
        count = 0
        for i in range(0, a):
            if q[k].iloc[i, 0].split(':')[0] == '1':
                out.append(q[k].iloc[i, 0].split(':')[1])
                count = count + 1
            #print(out)
            responses = []
        for i in range(0, count):
            try:
                params['q']=out[i]
                r = requests.get('https://southcentralus.api.cognitive.microsoft.com/luis/v2.0/apps/a4c959e5-3295-42da-ac2a-a84ead1b6f02',headers=headers, params=params)
                d = r.json()
                responses.append(d['topScoringIntent']['intent'])
                # print(d)
            except Exception as e:
                print('[Errno {0}] {1}'.format(e.errno, e.strerror))
        responses = Remove(responses)
        responses = [x for x in responses if x != 'Greetings' and x
                     != 'aboutbot' and x != 'Utilities.Stop' and x
                     != 'response']
        sol.append(responses)
    return sol
