# This Script is called from within node.js Script. It takes path as input and it predicts next best possible step(node).

# Note: Dont use Print statement in this program because print statement acts like output statement. After incurring print statement control shifts from python file to node.js file
# This Script only finds next steps 'id' . Steps are stored in the code folder itself.
from gremlin_python.driver import client
import sys, traceback,json
# Gremlin queries to fetch the required traversals
get_path="g.V(fromvertex).values('path')";
edge_label="g.V(fromvertex).outE().where(inV().has('id',tovertex))";
get_properties="g.V(fromvertex).values('name');"
edge_exist="g.V(fromvertex).outE().where(inV().has('id',tovertex))";
#b=['P1','P2','P3','P4'];
filter_edge="g.V(fromvertex).outE(edgelabel).has('path',within(aaa)).order().by('weight',decr).inV()";
#filter_edge="g.V(fromvertex).outE(.has('path',within(aaa)).order().by('weight',decr).inV()";
#aa="g.V('2').outE().hasId('66d47484-c0d3-49b3-a969-9b98d243208d').inV()";
client = client.Client('wss://graphapii.gremlin.cosmosdb.azure.com:443/','g', 
      username="/dbs/digitalwing/colls/digitalwing", 
      password="85WH3UCMt2FAqe9XjFAfTJ06DAR11nsJZStJViXN2oJgptPRy1SnePWHGumjG1x8knqxdge55CLT5RA0tRAxiQ==")

# Gremlin client to communicate with the Cosmos DB Graph API
def clientCall(client,query):
      callback = client.submitAsync(query);
      #print(callback.result());
      for result in callback.result():
          ##print(result);
          return result;
    
# To get all available path considering the previous path in mind    
def getPathValues(client,f,el):
        #print('finding path values');
        paths=[];
        paths.append(el);
        gp=get_path;
        gp=gp.replace('fromvertex','\''+f+'\'');
        res=clientCall(client,gp);
        #print('\n res---- ',res);
        for rst in res:
            rst1=rst.split(':');
            if len(rst1)>1 and rst1[0] == el:
                paths.append(rst1[1]);
        #print('available paths--- \n',paths);        
        return paths;        
        
# to display properties of the vertex        
def displayprop(client,id):
    gp=get_properties;
    gp=gp.replace('fromvertex','\''+id+'\'')
    result=clientCall(client,gp);                
    return result;

# to verify if any edge is there or not between 2 vertexes
def isEdge(client,f,t):
    global prevpath;
    isE=edge_exist;
    isE=isE.replace('fromvertex','\''+f+'\'');
    isE=isE.replace('tovertex','\''+t+'\'');
    result=clientCall(client,isE);
    if result is not None:
        return True;
    return False;

# to find the edge label between 'f' and 't' 
def edgeLabel(client,f,t):
    el=edge_label;
    el=el.replace('fromvertex','\''+f+'\'');
    el=el.replace('tovertex','\''+t+'\'');
    result1=clientCall(client,el);
    label=result1[0]['properties']['path'];
    #print(label);
    return label;

# this function parses the input arguments from the node.js Script.
def read_in():
    lines = sys.stdin.readlines()
    # Since our input would only be having one line, parse our JSON data from that
    #print(json.loads(lines[0]));
    #print(sys.argv[1]);
    return json.loads(lines[0])
completepath=[];
#completepath.append('Performance issue with games');
#next_node=str(sys.argv[1]);
##next_node=str('2C22085');
#completepath.append(next_node);
completepath=read_in();
#completepath=['Performance issue with games', '2C22085'];
el=edgeLabel(client,completepath[-2],completepath[-1]);
#print('\n',el); 
#input1=0;

# Execution starts from here
try:
    b= getPathValues(client,completepath[-1],el);
    #print('b==',b);
    c=filter_edge;
    d='';
    for i in b[0:-1]:  
        d=d+'\''+i+'\',';
    d=d+'\''+b[-1]+'\'';
    ##print('\n\nvalue of d====',d);
    c=c.replace('aaa',d);
    c=c.replace('fromvertex','\''+completepath[-1]+'\'');
    #print(c);
    # We also pass user response of question that is label of the edge.
    if sys.argv[1] =='No':
        c=c.replace('edgelabel',"'No','ways'");
    else:
        c=c.replace('edgelabel','\''+sys.argv[1]+'\'');
    ##print('\n filter edge query',c);
    vertex=clientCall(client,c);
    #print(vertex);
    parentissue=completepath[0];
    indexi=0;
    prop=vertex[indexi]['id'];
    while prop in completepath or not isEdge(client,parentissue,prop):
        prop=vertex[indexi]['id'];
        indexi=index+1;              
    if prop not in completepath and isEdge(client,parentissue,prop):
        print(prop);
    else :
        print(None);
    name=displayprop(client,prop);
   # #print(name);
    completepath.append(prop);
    #print(completepath);
    el=edgeLabel(client,completepath[-2],completepath[-1]);
    #input1= str(input('Enter to continue '));
    #input1=input1+1;
    #input1=input('Enter one number of u want to continue');
except Exception as e:
    sys.exit(1)    