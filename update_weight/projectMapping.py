# This script retrains the graph and updates the weights of edges. and it creates new edge based on previous chat samples
from gremlin_python.driver import client
import sys, traceback
import SequenceInPython 

#Gremlin queries to fetch required results 
edge_exist="g.V(fromvertex).outE().where(inV().has('id',tovertex))";
get_weight="g.V(fromvertex).outE().where(inV().has('id',tovertex)).values('weight')";
get_edgelabel="g.V(fromvertex).outE().where(inV().has('id',tovertex))"
update_weight="g.V(fromvertex).outE(edgelabel).as('e').inV().has('id',tovertex).select('e').property('weight',wd1)"
new_edge="g.V(fromvertex).addE(edgelabel).to(g.V(tovertex)).property('weight',0.0494).property('path',pathvalue)"
update_vertex="g.V(fromvertex).property(list,'path', pathname)";


#global prevlabel;

# Mapping of Graph vertex id's and the output of the LUIS so that we can map the sequence of troubleshooting steps fetched by the LUIS with the Graph vertex id's
dic={
		"Performance Games":"Performance issue with games",
		"performance_games_s1":"2C22085",
		"performance_games_s2":"1C22096.C18115",
		"performance_games_s3":"1C22096.C18173",
		"performance_games_s4":"0C22096.C21299",
		"performance_games_s5":"1C22096.C18172",
		"performance_games_s6":"1C22096.C18174",
		"performance_games_s7":"0C22096.C25797",
		"performance_games_s8":"1C22096.C25794",
		"performance_games_s9": "1C22096.C21299",
		"performance_games_s11": "1C22096.C18174",
		"performance_games_s12": "2C22096.C25797",
		"performance Start":"Windows Start",
		"windows_start_s1":"5C22085",
		"windows_start_s2":"1C22099.C18264",
		"windows_start_s3":"1C22099.C18263",
		"windows_start_s4":"1C22099.C18187",
		"windows_start_s5":"1C22099.C18188",
		"windows_start_s6":"0C22099.C25600",
		"windows_start_s7":"1C22099.C18189",
		"windows_start_s8":"1C22099.C25839",
		"windows_start_s9":"0C22101.C25498",
		"windows_start_s10":"0C22101.C25499",
		"windows_start_s11":"1C22101.C25500",
		"windows_start_s12":"1C22101.C28294",
		"windows_start_s13":"1C22101.C28295",
               }# still need to add remaining nodes to id mapping

chat_s=SequenceInPython.LUIS_IdentifiedSequence()
rate=[5,5,2,4,3,3,2,5]
# chat_s=[
# 		["Performance Games","performance_games_s1","performance_games_s3","performance_games_s9","performance_games_s5"],
# 		["Performance Games","performance_games_s1","performance_games_s3","performance_games_s4","performance_games_s6","performance_games_s7"],
# 		["Performance Games","performance_games_s1","performance_games_s5","performance_games_s6","performance_games_s11"],
# 		["Performance Games","performance_games_s5","performance_games_s1","performance_games_s3","performance_games_s9"],
# 		["Performance Games","performance_games_s5","performance_games_s1","performance_games_s3","performance_games_s4","performance_games_s2"],
# 		["Performance Games","performance_games_s4","performance_games_s2","performance_games_s6","performance_games_s7","performance_games_s5"],
# 		["Performance Games","performance_games_s4","performance_games_s2","performance_games_s6","performance_games_s11","performance_games_s1"],
# 		["Performance Games","performance_games_s4","performance_games_s1","performance_games_s2","performance_games_s5"]]


# sample previous chat samples to retrain the graph paths
# chat_s=[["performance Start","windows_start_s4","windows_start_s3","windows_start_s2"],
# 		["performance Start","windows_start_s4","windows_start_s3","windows_start_s2","windows_start_s8"],
# 		["performance Start","windows_start_s4","windows_start_s3","windows_start_s8"],
# 		["performance Start","windows_start_s2","windows_start_s11","windows_start_s8"],
# 		["performance Start","windows_start_s2","windows_start_s11","windows_start_s4","windows_start_s10"],
# 		["performance Start","windows_start_s2","windows_start_s11","windows_start_s4"],
# 		["performance Start","windows_start_s3","windows_start_s10","windows_start_s4"],
# 		["performance Start","windows_start_s3","windows_start_s10","windows_start_s2"],
# 		["performance Start","windows_start_s3","windows_start_s10","windows_start_s4","windows_start_s2"]]

n=len(dic.keys())+1;
m1=[];

# length of previous chat samples array
l=len(chat_s)
for i in range(0,l):
            m1.append(len(chat_s[i]))
print(m1[1])
grem_com=[]
#input1=chat_s[0];
#prevlabel='';

# gremlin client to send any query to the graph cosmos DB.
def clientCall(client,query):
      callback = client.submitAsync(query);
      print(callback.result());
      for result in callback.result():
          print(result);
          return result;

#calculating the weight where n= total no of nodes m=no of nodes suggested by agent
#r= rating for that conversation T= previous weightage  of the edge
def weightage(T,i,m,r):
            T=T+((1/n)*(2/m)*((m-i)/(m+1))*((r-3)/3))
            #new_weight= float(m)+0.1;
            return T;   
            
# to check if there is any edge between any 2 vertexes
def isEdge(client,f,t):
    global prevpath;
    isE=edge_exist;
    isE=isE.replace('fromvertex','\''+f+'\'');
    isE=isE.replace('tovertex','\''+t+'\'');
    result=clientCall(client,isE);
    if result is not None:
        prevpath=result[0]['properties']['path'];
        print("prevpath= ",prevpath);
        return True;
    return False;

# to get available path between 2 vertexes. it returns path value
def getPath(client,f,t):
    isE=edge_exist;
    isE=isE.replace('fromvertex','\''+f+'\'');
    isE=isE.replace('tovertex','\''+t+'\'');
    result=clientCall(client,isE);
    if result is not None:
        pathvalue=result[0]['properties']['path'];
        return pathvalue;

# to check if any existing path is there between 2 vertexes. return true if edge exists otherwise false
def pathExist(client,f,fromv,tov):
    isE="g.V(fromvertex).values('path')";
    isE=isE.replace('fromvertex','\''+f+'\'');
    result=clientCall(client,isE);
    print('\n result===-------',result);
    if result is not None:
        requiredpath= fromv+':'+tov;
        print('\nrequired path--------',requiredpath);
        print('\nduplicate exists or not ',requiredpath not in result);
        if requiredpath not in result:
            return True;
    return False;
    
# it fetches the edge properties and updates the weights depending on the weightage formula
def updateWeight(client,f,t,ii,m,r):
    gl=get_edgelabel;
    gl=gl.replace('fromvertex','\''+f+'\'');
    gl=gl.replace('tovertex','\''+t+'\'');
    result1=clientCall(client,gl);
    label=result1[0]['label'];
    print('\n')
    print(label);
    gw=get_weight;
    gw=gw.replace('fromvertex','\''+f+'\'');
    gw=gw.replace('tovertex','\''+t+'\'');
    mc=clientCall(client,gw);
    upW=update_weight;
    upW=upW.replace('fromvertex','\''+f+'\'');
    upW=upW.replace('tovertex','\''+t+'\'');
    upW=upW.replace('edgelabel','\''+label+'\'');
    upW=upW.replace('wd1',str(weightage(float(mc[0]),ii,m,r)));
    print('\n')
    print(upW);
    clientCall(client,upW);

#for creating new edge between two nodes 
def createEdge(client,f,t,cunt):
    ce=new_edge;
    ce=ce.replace('fromvertex','\''+f+'\'');
    ce=ce.replace('tovertex','\''+t+'\'');
    ce=ce.replace('edgelabel','\''+'ways'+'\'');
    ce=ce.replace('pathvalue','\''+'p'+str(cunt)+'\'');
    print('\n',ce);
    clientCall(client,ce);
    a=isEdge(client,f,t);
    print('value of a= ',a);
    #print(result11);    

# to update path property of the given vertex
def updatePropOfVertex(client,f,cunt):
    uv=update_vertex;
    uv=uv.replace('fromvertex','\''+f+'\'');
    print('-------',prevpath);
    uv=uv.replace('pathname','\''+prevpath+':'+'p'+str(cunt)+'\'')
    clientCall(client,uv);                

# to update path property of 'from' and 'to' vertex
def updatePropOfVertex1(client,f,frompath,topath):
    uv=update_vertex;
    uv=uv.replace('fromvertex','\''+f+'\'');
    #print('-------',prevpath);
    uv=uv.replace('pathname','\''+ frompath+':'+ topath+'\'')
    clientCall(client,uv);  


# Script execution starts from here
try:
    client = client.Client('wss://graphapii.gremlin.cosmosdb.azure.com:443/','g', 
      username="/dbs/digitalwing/colls/digitalwing", 
      password="85WH3UCMt2FAqe9XjFAfTJ06DAR11nsJZStJViXN2oJgptPRy1SnePWHGumjG1x8knqxdge55CLT5RA0tRAxiQ==")
    print("gremlin linked")
    count=1;
    rate_count=0
    # taking chat samples one by one
    for input11 in chat_s:
            # pathlist contains sequence of the paths taken by the agent(captured in chat sample)
            pathlist=[];
            input1=[dic[key1] for key1 in input11];
            final_input=input1;
            jj=1;
            r=rate[rate_count]
            rate_count=rate_count+1
            m=len(final_input);
            for ii in range(0,len(final_input)-1):
                #print('prev value',prevlabel);
                f=final_input[ii];
                t=final_input[jj];
                jj=jj+1; 
                # if edge is there then update the weight and append the new path value like p0:p1 means p1 is derived from p0
                if isEdge(client,f,t):
                    print('----- in if');
                    pathval=getPath(client,f,t);
                    updateWeight(client,f,t,ii,m,r);
                    if len(pathlist)==0:
                       pathlist.append(pathval);
                    else:
                        dup= pathExist(client,f,pathlist[-1],pathval);
                        if pathval != pathlist[-1] and dup:
                            updatePropOfVertex1(client,f,pathlist[-1],pathval);
                        pathlist.append(pathval);
                
                # if no edge is there then create new edge and update the weight and append the path value like p0:p1    
                else :
                    print('---- in else');
                    new_var= 'p'+str(count);
                    pathlist.append(new_var);
                    if prevpath != new_var:
                        updatePropOfVertex(client,f,count);
                    createEdge(client,f,t,count);
            count=count+1;   
except Exception as e:
    print('There was an exception: {0}'.format(e))
    traceback.print_exc(file=sys.stdout)
    sys.exit(1)
