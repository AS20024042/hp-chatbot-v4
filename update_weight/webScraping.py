
# coding: utf-8

# In[1]:


#fetching source code of webpage with the help of selenium webdriver 
from selenium import webdriver
import time
import io
id = 'http://hp.dezide.com/ts/start.jsp?guide=SlowComputerPerformance.net&section=external&language=english&lc=en&cc=us&tracking=spmrbfpa&dcsession=55373840&action=step&step=C22096.C21299&session=869a8ae8-5e31-4252-855a-07d0a6&response=0'
driver = webdriver.PhantomJS()
driver.get(id) 
contents = driver.page_source
print(contents)


# In[2]:


#importing beautiful Soup and creating an instance of soup
from bs4 import BeautifulSoup
soup = BeautifulSoup(contents,'html.parser')

#to check whether source code is complete or not(3-4 links means source code is  incomplete)
for link in soup.find_all('a'):
    print(link.get('href'))


# In[3]:


#Fetching Issue
Noboot1 = {}
key = soup.select('h1.guideName')[0].text.strip()
print (key)
Noboot1['Issue'] = 'Performance Games'


# In[4]:


#Fetching Content
cont = ''
key1= soup.select('div.currentStepName')[0].text.strip()
cont = key1
print cont
Noboot1['Content'] = cont
Noboot1['id'] = id


# In[5]:


#Fetching Steps
import re
stepslist = []
l = 0
m = 0
flag = True
findContent = soup.find_all('div',attrs={'class':'wa'})
if len(findContent)!=0:
    totalcontent = findContent[0]
    totalchilds = totalcontent.findChildren()
    l=0
    m=0
    flag=True
    for child in totalchilds:
        if child.name=='div' and child.parent.name=='div':
            stepslist.append(child.text)
        elif child.name=='li':
            s = ''
            liInside = child.findAll('li')
            InsideImg = child.findAll('img')
            if len(liInside)!=0:
                findliInside = child.find_all('li')
                findallimg  = child.find_all('img')
                l = len(findliInside)
                s = " "
                strtosplit = child.text
                #print strtosplit
                s = s+'* '+ ' '+ re.split('[.:]', strtosplit)[0]+ ' '
               # print s
                if l!=0 and flag==True:
                    for li in findliInside:
                            s = s+'....'+ ' ' + li.text+' '
                    #s = s + '* ' + child.text
                    flag=False
                if len(findallimg)!=0:
                    for img in findallimg:
                        s=s+img['src']+" "
                stepslist.append(s)        
            if len(liInside) == 0 and flag == True:
                string = ''
                findallimginside = child.find_all('img')
                #print findallimginside
                string = string +  '* ' + ' ' +child.text
                if len(findallimginside)!=0:
                    for img in findallimginside:
                        string = string+img['src']+'\n'
                stepslist.append(string)
            if len(liInside) ==0 and flag==False:
                l = l-1
                if l <= 0:
                    flag = True  
else:
    stepslist = []
print  stepslist
Noboot1['Steps'] = stepslist


# In[6]:


#Fetching question for next webpage or query
findques = soup.findAll('div',attrs={'id':'ddPrompt'})
if len(findques)!=0:
    key = soup.select('div#ddPrompt')[0].text.strip()
    #print(key)
    if len(key)!=0:
        Noboot1['Steps'].append(key)
    else:
        Noboot1['Steps'].append('')
print (Noboot1['Steps']) 


# In[7]:


#Fetching links for next webpage
weblink = []
options = {}
data = soup.findAll('div',attrs={'id':'aDD'})
for divs in data:
    data1 = divs.findAll('div')
    for div in data1:
        links = div.findAll('a')
        for a in links:
            options = {}
            key  = a.text
            options[key]=a['href']
            weblink.append(options) 
if len(weblink)==0:
    Noboot1['Next']=[{}]
else:
    Noboot1['Next']= weblink
print (Noboot1['Next'])  


# In[639]:


#Storing every json to local directory (change value of i )
import json
i = 47
Nobootjson1 = json.dumps(Noboot1)
print (Nobootjson1)
with io.open("new_" + str(i) + ".json", 'w') as f:
    f.write(unicode(json.dumps(Noboot1)))
    f.close() 

