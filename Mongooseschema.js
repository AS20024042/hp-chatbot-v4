const mongoose=require("mongoose");
const Schema=mongoose.Schema;
const transcriptschema=new Schema({
    from:{id:String,
            name:String},
    to:{id:String,
         name:String},
    message:String
});
const ConversationSchema=new Schema({
    id:{type:String},
    name:{type:String},
    channelId:{type:String},
    conversationId:{type:String},
    transcripts:[transcriptschema]
});
const Conversation=mongoose.model("Conversation",ConversationSchema);
const transcript=mongoose.model("transcript",transcriptschema);
module.exports={Conversation,transcript}