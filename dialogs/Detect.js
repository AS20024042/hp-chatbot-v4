/* This simple app uses the '/detect' resource to identify the language of
the provided text or texts. */

/* This template relies on the request module, a simplified and user friendly
way to make HTTP requests. */
const request = require("request");
const uuidv4 = require("uuid/v4");

var subscriptionKey = "97ac7f508c2647bb85ae6e10827494ca";

var endpoint =
  "https://api.cognitive.microsofttranslator.com/detect?api-version=3.0";
var a;

/* If you encounter any issues with the base_url or path, make sure that you are
using the latest endpoint: https://docs.microsoft.com/azure/cognitive-services/translator/reference/v3-0-detect */
const detect = function detectLanguage(tet) {
  return new Promise((resolve, reject) => {
    var subscriptionKey = "97ac7f508c2647bb85ae6e10827494ca";

    var endpoint =
      "https://api.cognitive.microsofttranslator.com/detect?api-version=3.0";
    var a;

    let options = {
      method: "POST",
      baseUrl: endpoint,
      url: "detect",
      qs: {
        "api-version": "3.0"
      },
      headers: {
        "Ocp-Apim-Subscription-Key": subscriptionKey,
        "Content-type": "application/json",
        "X-ClientTraceId": uuidv4().toString()
      },
      body: [
        {
          text: tet
          //text: "Salve, mondo!"
        }
      ],
      json: true
    };
    request(options, function(err, res, body) {
      //console.log(JSON.stringify(body, null, 4));
      a = body[0]["language"];
      //console.log("and the lang is " + a);
      resolve(a);
    });
  });
};

// Call the function to identify the language of the provided text.
//detectLanguage("bonjour");
module.exports.detect = detect;
