// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { TimexProperty } = require('@microsoft/recognizers-text-data-types-timex-expression');
const { ConfirmPrompt, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { ActivityHandler, ActionTypes, ActivityTypes, AttachmentLayoutTypes, CardFactory } = require('botbuilder');
const { ChoicePrompt, ComponentDialog, DialogSet, DialogTurnStatus } = require('botbuilder-dialogs');
//const { CancelAndHelpDialog } = require('./cancelAndHelpDialog');
//const { DateResolverDialog } = require('./dateResolverDialog');

const Medical_Dialog='Medical_Dialog'
const CONFIRM_PROMPT = 'confirmPrompt';
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
//const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';


class MedicalDialog extends ComponentDialog{
    constructor() {
        super(Medical_Dialog);

        // Define the main dialog and its related components.
        this.addDialog(new ChoicePrompt('cardPrompt'));
        this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.medicalChoice.bind(this),
            this.showMedicalCardStep.bind(this)
        ]));

        // The initial child Dialog to run.
        this.initialDialogId = WATERFALL_DIALOG;
    }



    //     this.addDialog(new TextPrompt(TEXT_PROMPT))
    //         .addDialog(new ConfirmPrompt(CONFIRM_PROMPT))
    //         .addDialog(new DateResolverDialog(DATE_RESOLVER_DIALOG))
    //         .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
    //             this.destinationStep.bind(this),
    //             this.originStep.bind(this),
    //             this.travelDateStep.bind(this),
    //             this.confirmStep.bind(this),
    //             this.finalStep.bind(this)
    //         ]));

    //     this.initialDialogId = WATERFALL_DIALOG;
    // }

    /**
     * If a destination city has not been provided, prompt for one.
     *
     */

async medicalChoice(stepContext)
{
    console.log('MedicalDialog.choiceCardStep');

        // Create the PromptOptions which contain the prompt and re-prompt messages.
        // PromptOptions also contains the list of choices available to the user.
        const options = {
            prompt: 'What is the emergency about?',
            retryPrompt: 'That was not a valid choice, please choose the emergency type.',
            choices: this.getMedicalChoices()
                        };

        // Prompt the user with the configured PromptOptions.
        return await stepContext.prompt('cardPrompt', options);
    }


async showMedicalCardStep(stepContext) {
    console.log('MainDialog.showMedicalCardStep');

    switch (stepContext.result.value) {
    case 'Cardiac Arrest':
            console.log("Cardiac Arrest")
            await stepContext.context.sendActivity("Steps for Dealing with cardiac arrest will be shown");
            // await stepContext.context.sendActivity({ attachments: [this.createHeroCard()]});
            // await stepContext.context.sendActivity(this.creatEmergency_Contacts());
            // await stepContext.context.sendActivity({ attachments: [this.createThumbnailCard()]});
            break;
    case 'Leg Injury':
            await stepContext.context.sendActivity("Steps for dealing with leg injury will be shoown");
            break;
    case 'Other':
            await stepContext.context.sendActivity("Sorry, I can't help you with this. Please contact our emergency service team-+17129345670");
            break;
    default:
                await stepContext.context.sendActivity("Please select the valid answer");
                break;
            }
            return await stepContext.endDialog();
        }

getMedicalChoices() {
    const cardOptions = [
        {
            value: 'Cardiac Arrest',
            synonyms: ['Cardiac']
        },
        {
            value: 'Leg Injury',
            synonyms: ['Leg']
        },
        {
            value: 'Other',
            synonyms: ['Other']
        } ];
        return cardOptions;
    }

createCardiacHeroCard() {
    // return CardFactory.heroCard(
    //     'Evacuation Plan',
    //     CardFactory.images(['./evac_plan.png']),
    //     CardFactory.actions([
    //         {
    //             type: 'openUrl',
    //             title: 'Get started',
    //             value: 'https://docs.microsoft.com/en-us/azure/bot-service/'
    //         }
    //     ])
    // );
    return CardFactory.heroCard(
        'Evacuation Plan',
        ['http://localhost:3978/evac_plan.png']
        //['buy']
   );
}

creatEmergency_Contacts()
{
    const reply = { type: ActivityTypes.Message };
    const buttons = [
        { type: ActionTypes.ImBack, title: 'Building Emergency Services- +129345670'},
        { type: ActionTypes.ImBack, title: 'Fire Service- 101'},
        { type: ActionTypes.ImBack, title: 'Ambulance- 102'},
        { type: ActionTypes.ImBack, title: 'Disaster Management Services- 108'}
    ];
    const card = CardFactory.heroCard('', undefined,
    buttons, { text: 'Contact below phone numbers in case of high emergency:' });
    reply.attachments = [card];
    return reply
}

createThumbnailCard() {
    return CardFactory.thumbnailCard(
        'Way to use Fire Extinguisher',
        [{ url: 'http://localhost:3978/fire_ex.png' }],
        [{
            type: 'openUrl',
            title: 'Watch Video now',
            value: 'https://www.youtube.com/watch?v=w4jHpHoYZhk'
        }],
        {
            subtitle: 'Use Fire Extinguisher',
            text: 'If less fire, use fire extinguisher kept at every exit point shown in the emergency exit route map.'
        }
    );
}




    // async destinationStep(stepContext) {
    //     const bookingDetails = stepContext.options;

    //     if (!bookingDetails.destination) {
    //         return await stepContext.prompt(TEXT_PROMPT, { prompt: 'To what city would you like to travel?' });
    //     } else {
    //         return await stepContext.next(bookingDetails.destination);
    //     }
    // }

    /**
     * If an origin city has not been provided, prompt for one.
     */
    // async originStep(stepContext) {
    //     const bookingDetails = stepContext.options;

    //     // Capture the response to the previous step's prompt
    //     bookingDetails.destination = stepContext.result;
    //     if (!bookingDetails.origin) {
    //         return await stepContext.prompt(TEXT_PROMPT, { prompt: 'From what city will you be travelling?' });
    //     } else {
    //         return await stepContext.next(bookingDetails.origin);
    //     }
    // }

    /**
     * If a travel date has not been provided, prompt for one.
     * This will use the DATE_RESOLVER_DIALOG.
     */
    // async travelDateStep(stepContext) {
    //     const bookingDetails = stepContext.options;

    //     // Capture the results of the previous step
    //     bookingDetails.origin = stepContext.result;
    //     if (!bookingDetails.travelDate || this.isAmbiguous(bookingDetails.travelDate)) {
    //         return await stepContext.beginDialog(DATE_RESOLVER_DIALOG, { date: bookingDetails.travelDate });
    //     } else {
    //         return await stepContext.next(bookingDetails.travelDate);
    //     }
    // }

    /**
     * Confirm the information the user has provided.
     */
    // async confirmStep(stepContext) {
    //     const bookingDetails = stepContext.options;

    //     // Capture the results of the previous step
    //     bookingDetails.travelDate = stepContext.result;
    //     const msg = `Please confirm, I have you traveling to: ${ bookingDetails.destination } from: ${ bookingDetails.origin } on: ${ bookingDetails.travelDate }.`;

    //     // Offer a YES/NO prompt.
    //     return await stepContext.prompt(CONFIRM_PROMPT, { prompt: msg });
    // }

    // /**
    //  * Complete the interaction and end the dialog.
    //  */
    // async finalStep(stepContext) {
    //     if (stepContext.result === true) {
    //         const bookingDetails = stepContext.options;

    //         return await stepContext.endDialog(bookingDetails);
    //     } else {
    //         return await stepContext.endDialog();
    //     }
    // }

    // isAmbiguous(timex) {
    //     const timexPropery = new TimexProperty(timex);
    //     return !timexPropery.types.has('definite');
    // }
}

module.exports.MedicalDialog = MedicalDialog;
module.exports.Medical_Dialog = Medical_Dialog;