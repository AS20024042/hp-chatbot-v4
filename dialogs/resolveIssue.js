// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
var config = require("./config");
var https = require("https");
var async = require("async");
var removeValue = require("remove-value");
const py = require("./python.js");
const fs = require("fs");
const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");
const display_steps = "display_steps";
const resolve_issue = "resolve_issue";
const CONFIRM_PROMPT = "confirmPrompt";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const { ShowSteps, show_steps } = require("./showSteps.js");
const { ConfirmSteps, confirm_steps } = require("./confirmSteps.js");
var PythonShell = require("python-shell");
var unique = require("array-unique");
var Gremlin = require("gremlin");
var dirpath = require("path");
var message;
var replyback;
var arg;

class ResolveIssue extends ComponentDialog {
  constructor() {
    super(resolve_issue);

    // Define the main dialog and its related components.
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new ConfirmSteps());
    this.addDialog(new ShowSteps());
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [this.evacuationPath.bind(this)])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async evacuationPath(stepContext) {
    console.log("================");
    console.log("entered resolve dialog");
    console.log("================");
    console.log("");
    var dat = stepContext.options.dat;
    arg = stepContext.options.arg;
    console.log("at resolve " + dat.results);
    console.log("at resolve complete path " + dat.completepath);

    await py
      .py(dat, arg)
      .then(function(value) {
        console.log(" this from the promise====  " + value);
        return value;
      })
      .then(function(value) {
        dat.latestnode = value;
        dat.completepath.push(value);
        console.log(dat.latestnode + "=======" + dat.completepath);
        var filename = "./jsons/" + dat.latestnode + ".json";
        let f2 = fs.readFileSync(filename);
        var jsc;
        eval("jsc = " + f2);
        var key1 = "Steps_en";
        dat.datajson = jsc;
        dat.text = jsc[key1];
        dat.splittext = dat.text;
        dat.count = 0;
        if (dat.text == null) {
          //await stepContext.context.sendActivity("Run Contact Dialog");
          console.log("contact dialog at resolve issue");
        } else {
          // return stepContext.beginDialog(display_steps, {
          //   dat: dat
          // });
          console.log("==============================");
          console.log(dat.text);
          console.log("===============================");
        }
      });
    return await stepContext.replaceDialog(display_steps, {
      dat: dat
    });

    //return await stepContext.endDialog();
  }
}

module.exports.ResolveIssue = ResolveIssue;
module.exports.resolve_issue = resolve_issue;
