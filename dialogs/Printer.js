const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { gamecard } = require("../cards/gameper");
const fs = require("fs");
const Performance_Dialog = "Performance_Dialog";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const Search = require("./bingsearch");
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { Feedback, feedback } = require("./Feedback");
const { Contact, contact } = require("./Contact.js");
const {
  Printer_Wifi_Dialog,
  Printer_Wifi_Issue
} = require("./Printer_Wifi_Issue");
var dat;
const Printer_Issue = "Printer_Issue";
const {
  Printer_Job_Stuck_Issue,
  Printer_Job_Stuck_Dialog
} = require("./Printer_Job_Stuck.js");
const {
  Printer_Offline_Issue,
  Printer_Offline_Dialog
} = require("./Printer_Offline.js");
const {
  Printer_Look_Dialog,
  Printer_Look_Issue
} = require("./Printer_Look.js");
const { MainDialog2, Main_Dialog2 } = require("./mainDialog2");
var a, b, c, d, aa, bb, cc, dd, msg;

class PrinterDialog extends ComponentDialog {
  constructor(conversationState, userState) {
    super(Printer_Issue);

    // The state management objects for the conversation and user state.
    this.conversationState = conversationState;
    this.userState = userState;

    // Define the main dialog and its related components.

    this.addDialog(new Feedback());
    this.addDialog(new Contact());
    this.addDialog(new DisplaySteps());
    this.addDialog(new MainDialog2());
    this.addDialog(new Printer_Wifi_Dialog());
    this.addDialog(new Printer_Job_Stuck_Dialog());
    this.addDialog(new Printer_Offline_Dialog());
    this.addDialog(new Printer_Look_Dialog());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.iniStep.bind(this),
        this.initialStep.bind(this),
        this.performanceStep.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }
  async iniStep(stepContext) {
    dat = stepContext.options.dat;
    console.log("====================");
    console.log("inside main PRINTER");
    console.log("====================");
    var lol =
      "Please confirm the printer model and serial number. This information can be found on the back of the printer on a white bar-coded sticker. ";
 
    if (dat.lang != "en") {
      await Translate.translate(lol, dat.lang).then(function(value) {
        console.log("converterd " + value);
        lol = value;
      });
    }
    await stepContext.context.sendActivity(lol);
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Image",
              altText: "",
              url: "https://i.imgur.com/iNw1H0r.png"
              // "https://hp.dezide.com/data/pictures/hp-external/web_author/guid-4ded5cad-ffb6-4783-bdf9-3719ade50ed7-low_55920_en.jpg"
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    return await stepContext.prompt("textPrompt", {});
  }

  async initialStep(stepContext) {
    dat.printer_name = stepContext.result;
    console.log("printer name===",dat.printer_name);
    //await stepContext.context.sendActivity("enter the querry");
    msg = "Assistance with " + dat.printer_name + ":";
    a = aa = "wifi issue";
    b = bb = "Print jobs are getting stuck";
    c = cc = "Printouts not look good";
    d = dd = "Printer is offline";
    if (dat.lang != "en") {
      await Translate.translate(msg, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg = value;
      });
      await Translate.translate(a, dat.lang).then(function(value) {
        console.log("converterd " + value);
        a = value;
      });
      await Translate.translate(b, dat.lang).then(function(value) {
        console.log("converterd " + value);
        b = value;
      });
      await Translate.translate(c, dat.lang).then(function(value) {
        console.log("converterd " + value);
        c = value;
      });
      await Translate.translate(d, dat.lang).then(function(value) {
        console.log("converterd " + value);
        d = value;
      });
    }
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: a,
                      data: a
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: b,
                  data: b
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: c,
                  data: c
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: d,
                  data: d
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });

    return await stepContext.prompt("textPrompt", {});
  }
  async performanceStep(stepContext) {
    console.log(stepContext.result + "printer");
    if (dat.lang != "en") {
      if (stepContext.result == a) {
        dat.printer = aa;
      } else if (stepContext.result == b) {
        dat.printer = bb;
      } else if (stepContext.result == c) {
        dat.printer = cc;
      } else if (stepContext.result == d) {
        dat.printer = dd;
      } else {
        dat.printer = stepContext.result;
      }
    } else {
      dat.printer = stepContext.result;
    }

    console.log("is the printer issue == " + dat.printer);

    if (dat.printer == "wifi issue") {
      return await stepContext.replaceDialog(Printer_Wifi_Issue, {
        dat: dat
      });
    } else if (dat.printer == "Print jobs are getting stuck") {
      console.log("stuck");
      return await stepContext.replaceDialog(Printer_Job_Stuck_Issue, {
        dat: dat
      });
    } else if (dat.printer == "Printouts dont look good") {
      console.log("looks good");
      return await stepContext.replaceDialog(Printer_Look_Issue, {
        dat: dat
      });
    } else if (dat.printer == "Printer is offline") {
      console.log("offline");
      return await stepContext.replaceDialog(Printer_Offline_Issue, {
        dat: dat
      });
    } else {
      dat.main = dat.printer;
      return await stepContext.replaceDialog(Main_Dialog2, {
        dat: dat
      });
    }
  }
}
module.exports.PrinterDialog = PrinterDialog;
module.exports.Printer_Issue = Printer_Issue;
