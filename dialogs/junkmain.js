// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");

const CONFIRM_PROMPT = "confirmPrompt";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
const TEXT_PROMPT = "textPrompt";
const feedback = "feedback";
const CHOICE_PROMPT = "choicePrompt";
const feedbackcard = require("../cards/feedbackcard.json");
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
//const { DisplaySteps, display_steps } = require("./displaysteps.js");
var dat;
const testfeedback = require("../cards/testfeedback.json");

class Feedback extends ComponentDialog {
  constructor() {
    super(feedback);

    // Define the main dialog and its related components.
    // this.addDialog(new DisplaySteps());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    //this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.feed_first.bind(this),
        this.feed_second.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async feed_first(stepContext) {
    console.log("feedback first");
    dat = stepContext.options.dat;
    // let options = MessageFactory.attachment(
    //   CardFactory.adaptiveCard(feedbackcard)
    // );
    // return await stepContext.prompt(TEXT_PROMPT, {
    //   prompt: options
    // });

    let company_card = MessageFactory.attachment(
      CardFactory.adaptiveCard(testfeedback)
    );

    return await stepContext.prompt(TEXT_PROMPT, {
      prompt: company_card
    });
    //return await stepContext.next();
    // await stepContext.context.sendActivity({
    //   text: "",
    //   attachments: [
    //     CardFactory.adaptiveCard({
    //       type: "AdaptiveCard",
    //       version: "1.0",
    //       body: [
    //         {
    //           type: "Container",
    //           items: [
    //             {
    //               type: "TextBlock",
    //               text: "feedback form"
    //             },
    //             {
    //               type: "ActionSet",
    //               actions: [
    //                 {
    //                   type: "Action.Submit",
    //                   title: "⭐",
    //                   data: "1"
    //                 }
    //               ]
    //             }
    //           ]
    //         },
    //         {
    //           type: "ActionSet",
    //           actions: [
    //             {
    //               type: "Action.Submit",
    //               title: "⭐⭐",
    //               data: "2"
    //             }
    //           ]
    //         },
    //         {
    //           type: "ActionSet",
    //           actions: [
    //             {
    //               type: "Action.Submit",
    //               title: "⭐⭐⭐",
    //               data: "3"
    //             }
    //           ]
    //         },
    //         {
    //           type: "ActionSet",
    //           actions: [
    //             {
    //               type: "Action.Submit",
    //               title: "⭐⭐⭐⭐",
    //               data: "4"
    //             }
    //           ]
    //         },
    //         {
    //           type: "ActionSet",
    //           actions: [
    //             {
    //               type: "Action.Submit",
    //               title: "⭐⭐⭐⭐⭐",
    //               data: "5"
    //             }
    //           ]
    //         }
    //       ],
    //       $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
    //     })
    //   ]
    // });
    // console.log("displayed card");

    // return await stepContext.prompt("textPrompt", {});
  }
  async feed_second(stepContext) {
    console.log("enter feedback second");
    console.log(stepContext.result);
    console.log(stepContext.context.activity.value.id_text);
    console.log(stepContext.activity.value);
    var json = JSON.stringify(stepContext.activity.value);
    turnContext.activity.text = json;
    console.log(turnContext.activity.text);
    console.log(stepContext.result);
  }
}

module.exports.Feedback = Feedback;
module.exports.feedback = feedback;
/* {
            type: "AdaptiveCard",
            version: "1.0",
            body: [
              {
                type: "Container",
                items: [
                  {
                    type: "TextBlock",
                    text: "feedback form"
                  },
                  {
                    type: "ActionSet",
                    actions: [
                      {
                        type: "Action.Submit",
                        title: "⭐",
                        data: "1"
                      }
                    ]
                  }
                ]
              },
              {
                type: "ActionSet",
                actions: [
                  {
                    type: "Action.Submit",
                    title: "⭐⭐",
                    data: "2"
                  }
                ]
              },
              {
                type: "ActionSet",
                actions: [
                  {
                    type: "Action.Submit",
                    title: "⭐⭐⭐",
                    data: "3"
                  }
                ]
              },
              {
                type: "ActionSet",
                actions: [
                  {
                    type: "Action.Submit",
                    title: "⭐⭐⭐⭐",
                    data: "4"
                  }
                ]
              },
              {
                type: "ActionSet",
                actions: [
                  {
                    type: "Action.Submit",
                    title: "⭐⭐⭐⭐⭐",
                    data: "5"
                  }
                ]
              }
            ],
            $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
          }
           */
