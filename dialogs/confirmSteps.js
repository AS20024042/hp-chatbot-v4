// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const CHOICE_PROMPT = "choicePrompt";
const { gamecard } = require("../cards/gameper");
const Translate = require("./Translate.js");
const fs = require("fs");
const Performance_Dialog = "Performance_Dialog";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG_1 = "WATERFALL_DIALOG_1";
//const { CancelAndHelpDialog } = require('./cancelAndHelpDialog');
//const { DateResolverDialog } = require('./dateResolverDialog');
//const TEXT_PROMPT = "TEXT_PROMPT";
const { UserProfile } = require("../userProfile");
const confirm_steps = "confirm_steps";
const CONFIRM_PROMPT = "confirmPrompt";
var dat, yes, no, yes1, no1;
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
//const TEXT_PROMPT = 'textPrompt';

const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { ShowSteps, show_steps } = require("./showSteps.js");
class ConfirmSteps extends ComponentDialog {
  constructor() {
    super(confirm_steps);

    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    //this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG_1, [
        this.confirmStep.bind(this),
        this.confirmSecondPath.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG_1;
  }

  async confirmStep(stepContext) {
    console.log("initial confirm step");
    dat = stepContext.options.dat;
    console.log(dat.latestnode);
    var msg = "If you have performed the above steps, click yes";
    //const promptOptions = {prompt: "If you have performed the above steps, Type Yes", retryPrompt: 'The answer you give should be in Yes or No.'};
    // return await stepContext.prompt(TEXT_PROMPT, promptOptions);

    //return await stepContext.prompt(CONFIRM_PROMPT, { prompt: msg });

    yes = yes1 = "yes";
    no = no1 = "no";

    if (dat.lang != "en") {
      await Translate.translate(yes, dat.lang).then(function(value) {
        console.log("converterd " + value);
        yes = value;
      });
      await Translate.translate(no, dat.lang).then(function(value) {
        console.log("converterd " + value);
        no = value;
      });
      await Translate.translate(msg, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg = value;
      });
    }

    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: yes,
                      data: yes
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: no,
                  data: no
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });

    return await stepContext.prompt("textPrompt", {});
  }

  async confirmSecondPath(stepContext) {
    console.log("Starting confirm path step=====");
    console.log(stepContext.result);

    if (stepContext.result == yes) {
      //  console.log("starting showsteps dialog ");
        return await stepContext.replaceDialog("show_steps", {
        dat: dat
      });
    } else if (stepContext.result == no) {
      var str_confirm = "Ok, We'll resume once you complete the steps";
      await stepContext.context.sendActivity(str_confirm);
      return await stepContext.replaceDialog("confirm_steps", {
        dat: dat
      });
    } else {
      dat.main = stepContext.result;
      console.log("================");
      console.log("from Confirsteps");
      console.log("================");
      return await stepContext.replaceDialog("Main_Dialog2", {
        dat: dat
      });
    }
    // {
    //   await stepContext.context.sendActivity("Hurrah");
    //   //  return await stepContext.endDialog();
    // } else
    // {
    //   await stepContext.context.sendActivity("LOL");

    // }
  }
  
}

module.exports.ConfirmSteps = ConfirmSteps;
module.exports.confirm_steps = confirm_steps;
