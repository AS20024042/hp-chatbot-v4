
var options = 
{
    mode: 'text',
    pythonPath: 'py/python364x64/python',
    pythonOptions: ['-u'], // get print results in real-time
    args: ['No']

};



//Translator Start
//Resource Name : digitalwingTRanslatorv3

function translate_lang(textgiven,userLang)//to convert LUIS output to user's language
{  
    //console.log("inside translation")
    return new Promise((resolve, reject) => {
        var ltd = userLang;

        //ltd.trim();
        console.log("value of ltd", userLang);
        if(ltd=="en")
        {
            resolve(textgiven);
        }
        
        else{
            let subscriptionKey = '97ac7f508c2647bb85ae6e10827494ca';

        let host = 'api.cognitive.microsofttranslator.com';
        let path = '/translate?api-version=3.0';

        // Translate to German and Italian.
        let params = '&to='+userLang;

        let text = textgiven;

        let response_handler = function (response) {
        let body = '';
        response.on ('data', function (d) {
        body += d;
            });
        response.on ('end', function () {
            //let json = JSON.stringify(body)
            let json1 =JSON.parse(body);
            console.log(json1);
            console.log("Translated text in new method",json1[0].translations[0].text);
            resolve(json1[0].translations[0].text)
        });
        response.on ('error', function (e) {
        console.log ('Error: ' + e.message);
            });
};


let Translate = function(content) {
    let request_params = {
        method : 'POST',
        hostname : host,
        path : path + params,
        headers : {
            'Content-Type' : 'application/json',
            'Ocp-Apim-Subscription-Key' : subscriptionKey
           }
    };

    let req = https.request(request_params, response_handler);
    req.write(content);
    req.end();
}

let content = JSON.stringify ([{'Text' : text}]);
Translate(content)

            
        }
        
    })                   //return data1;
}

function translate_lang_prompts(textgiven,userLang) //to transalate strings into english for LUIS to understand
{
    return new Promise((resolve, reject) => {
        //ltd = s;
        //ltd.trim();
        
       
       console.log("inside translate_lang_prompts");
       if (userLang=="en") {
           resolve(textgiven)
       }
       else{
           
           
            langdetect = "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0" +
                     "?text=" + encodeURIComponent(textgiven) + "&to=en" 
          request.get({
            url : langdetect,
            category:'generalnn',
            headers:{
              'Ocp-Apim-Subscription-Key' : '97ac7f508c2647bb85ae6e10827494ca',
            },
            time : true
          },
          function(err, response,body)
          {
              var x = response.body;
              s = x.trim();
              s = s.replace(/^"(.*)"$/, '$1');
              console.log("response is", s);
              console.log("Request time in ms for translation", response.elapsedTime);
              console.log('statusCode:', response.statusCode);
              resolve(s)
          })
       
       }
        
        //return data1;
    })
}


var issueMapping = new Array();
issueMapping['no Boot'] = '';
issueMapping['Performance application'] = '';
issueMapping['Performance General'] = '';
issueMapping['Performance Games'] = 'Performance issue with games';
issueMapping['Update'] = '';
issueMapping['Performance Start'] = 'Windows Start';

var cho = new Array();
cho['Check system requirement'] = "2C22085";
cho['Computer over heating'] = "0C22096.C21299";
cho['Update drivers'] = "1C22096.C18172";
cho['Not Sure!'] = "2C22085";

cho['Do you have many application in your system?'] = "1C22099.C18187";
cho['Perform hard reset'] = "1C22099.C18264";
cho['Update drivers!'] = "1C22099.C18263";
cho['Not Sure'] = "5C22085";


var chonew1 = new Array("2C22085", "0C22096.C21299", "1C22096.C18172", "2C22085");
var chonew2 = new Array("1C22099.C18187", "1C22099.C18264", "1C22099.C18263", "5C22085");


var replyback = '';

function pythontalking(aaa, callback) {
    var pyshell = new PythonShell('update_weight/project_traversing.py', options);
    console.log(aaa);
    pyshell.send(JSON.stringify(aaa));
    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        //console.log(message);
        message = message.replace('\r', '');

        replyback = message;

    });

    // end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err) {
            // throw err;
            callback();
        } else {
            console.log('finished');
            callback(replyback);
        }
    });
};

function getcontents(id, callback) {
    let get_steps = "g.V(id1).values('content')";
    get_steps = get_steps.replace('id1', '\'' + id + '\'');
    console.log(get_steps);
    global.client1.execute(get_steps, {}, (err, results) => {
        if (err) {
            return callback(console.error(err));
        }

        console.log("Result: %s\n", JSON.stringify(results));
        callback(results[0])
    });
}

function getsteps(id, callback) {
    let get_steps = "g.V(id1).values('steps')";
    get_steps = get_steps.replace('id1', '\'' + id + '\'');
    console.log(get_steps);
    global.client1.execute(get_steps, {}, (err, results) => {
        if (err) {
            return callback(console.error(err));
        }

        console.log("Result: %s\n", JSON.stringify(results));
        callback(results[0])
    });
}

function getedgelabels(id, callback) {
    let getlabels = "g.V(id1).outE().values('label')"
    getlabels = getlabels.replace('id1', '\'' + id + '\'');
    console.log(getlabels);
    global.client1.execute(getlabels, {}, (err, results) => {
        if (err) {
            return callback(console.error(err));
        }

        console.log("Result: %s\n", JSON.stringify(results));

        callback(results)
    });
}



var chonew = [];

lib.dialog('/', [
    function (session) {
        session.conversationData.userInfo.userChoice = [];

        if (session.conversationData.userInfo.topIntentName == 'Performance Start') 
        {
            chonew = chonew2;
            //builder.Prompts.choice(session, 'Please select one of the following', "I have a lot of \napplications installed|I can perform \na hard reset|I think I haven't \nupdated drivers in a \nlong while|Not Sure", { listStyle: builder.ListStyle.button, retryPrompt: 'Please select your detailed problem?' });  
            var demoarray2 = new Array("I have a lot of \napplications installed", 'I can perform \na hard reset', "I think I haven't \nupdated drivers in a \nlong while", 'Not Sure', "Please select one of the following");
            async.mapSeries(demoarray2, function (key12, callback2) {
                translate_lang(key12, session.conversationData.userInfo.language).then(function (response1) {
                    console.log("response from translate\n", response1);
                    session.conversationData.userInfo.userChoice.push(response1);
                    // builder.Prompts.choice(session, 'Please select your elaborated problem?',  demoarray , { listStyle: builder.ListStyle.button,retryPrompt:"Please select one of the following" });
                    if (demoarray2.indexOf(key12) == demoarray2.length - 1) {
                        console.log('hip hip hurray');
                        builder.Prompts.choice(session, session.conversationData.userInfo.userChoice[4], session.conversationData.userInfo.userChoice.slice(0, 4), {
                            listStyle: builder.ListStyle.button,
                            retryPrompt: session.conversationData.userInfo.userChoice[4]
                        });
                    }
                    callback2();
                });

            })
        } else {
            //builder.Prompts.choice(session, 'Please select one of the following', "My system might not be meeting minimum requirements|My laptop over heats|I think I haven't updated drivers in a long while|Not Sure", { listStyle: builder.ListStyle.button, retryPrompt: 'Please select your detailed problem?' } );
            chonew = chonew1;
            var demoarray1 = new Array("System minimum requirements not met", "My laptop over heats", "I think I haven't updated drivers", "Not Sure", "Please select one of the following");
            async.mapSeries(demoarray1, function (key11, callback2) {
                translate_lang(key11, session.conversationData.userInfo.language).then(function (response1) {
                    console.log("response from translate\n", response1);
                    session.conversationData.userInfo.userChoice.push(response1);
                    // builder.Prompts.choice(session, 'Please select your elaborated problem?',  demoarray , { listStyle: builder.ListStyle.button,retryPrompt:"Please select your elaborated problem?" });
                    if (demoarray1.indexOf(key11) == demoarray1.length - 1) {
                        console.log('hip hip hurray');
                        builder.Prompts.choice(session, session.conversationData.userInfo.userChoice[4], session.conversationData.userInfo.userChoice.slice(0, 4), {
                            listStyle: builder.ListStyle.button,
                            retryPrompt: session.conversationData.userInfo.userChoice[4]
                        });
                    }
                    callback2();
                });
            })

        }
    },
    
    function (session, results) {
        if (chonew[session.conversationData.userInfo.userChoice.indexOf(results.response.entity)] != null) {
            var position = session.conversationData.userInfo.userChoice.indexOf(results.response.entity);
            console.log('*************************', position);

            session.conversationData.userInfo.latestnode = chonew[position];
            session.conversationData.userInfo.completepath.push(session.conversationData.userInfo.latestnode);
            // getsteps(session.conversationData.userInfo.latestnode, function (text) {
            var filename = "./jsons/" + session.conversationData.userInfo.latestnode + ".json";
            let f2 = fs.readFileSync(filename);
            eval("var jsc = " + f2);
            session.conversationData.userInfo.text = [];
            var key1 = "Steps_" + session.conversationData.userInfo.language.replace(/['"]+/g,  '');
            session.conversationData.userInfo.datajson = jsc;
            session.conversationData.userInfo.text = jsc[key1];

            if (session.conversationData.userInfo.text == null) {
                session.beginDialog('*:contact');
            } else {
                // session.conversationData.userInfo.splittext = text.split('\n\n');
                session.conversationData.userInfo.splittext = session.conversationData.userInfo.text;

                //getcontents(session.conversationData.userInfo.latestnode, function (content) {
                console.log(session.conversationData.userInfo.text);
                // session.conversationData.userInfo.content = content;
                if (session.conversationData.userInfo.latestnode == '2C22085' && session.conversationData.userInfo.entitytype == "games") {

                    session.beginDialog('basic:bingsearch');
                }
                //var len = session.conversationData.userInfo.splittext.length;
                session.conversationData.userInfo.count = 0;
                //builder.Prompts.text(session, "Are you ready?");
                session.sendTyping();
                var str28 = 'Showing you steps. Please follow as per instructions';
                return translate_lang(str28, session.conversationData.userInfo.language)
                    .then(function (str2_ans) {
                        session.send(str2_ans);
                        session.sendTyping();
                        var key2 = "Content_" + session.conversationData.userInfo.language.replace(/['"]+/g,  '');
                        session.send(jsc[key2]);
                        //session.send(session.conversationData.userInfo.content);
                        session.beginDialog('basic:displaysteps');
                    });
            }
            // });
            //session.beginDialog('resolveIssue');
        }
    }
]).beginDialogAction('*:anger', '*:anger', {
        matches: 'anger'
    })
    .beginDialogAction('*:check', '*:check', {
        matches: 'handoff'
    })
    //.beginDialogAction('greetings', 'greetings', { matches : 'Greetings'})
    .beginDialogAction('*:qnad', '*:kbase', {
        matches: 'qna'
    })
    // .beginDialogAction('*:showVideo', '*:showVideo', {
    //     matches: 'showVideo'
    // })
     .beginDialogAction('*:service_center', '*:service_center', {
        matches: 'locationServiceCenter'
    });


var subscriptionKey = process.env.searchkey;
var customConfigId = process.env.customConfigid;
var host = 'api.cognitive.microsoft.com';
var path = '/bingcustomsearch/v7.0/search';

//bing search
lib.dialog('bingsearch', [
    function (session, args, next) { // for the hero card.
        searchTerm = "what is the laptop requirement for" + session.conversationData.userInfo.entity_name; // the top intent is stored in the variable term
        let searchResponse = '';
        var options = {
            url: 'https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/search?' +
                'q=' + searchTerm +
                '&customconfig=' + customConfigId,
            headers: {
                'Ocp-Apim-Subscription-Key': subscriptionKey
            }
        }
        request(options, function (error, response, body) {
            searchResponse = JSON.parse(body);
            console.log(searchResponse.webPages.value[0].url);
            url = searchResponse.webPages.value[0].url;
            ab = searchResponse.webPages.value[0].snippet;
            url1 = searchResponse.webPages.value[1].url;
            url2 = searchResponse.webPages.value[2].url;
            ab1 = searchResponse.webPages.value[1].snippet;
            ab2 = searchResponse.webPages.value[2].snippet;
        });
        // session.send(url1 + " " + ab1 + url2 + " " + ab2 + url3 + " " + ab3);  
    },
    function (session, results) {
        session.beginDialog("basic:search_cards");
    }
]);

lib.dialog('search_cards', [
    function (session) {
        var msg = new builder.Message(session);
        msg.attachmentLayout(builder.AttachmentLayout.carousel)
        msg.attachments([
            new builder.HeroCard(session)
            .title('Check game requirements here')
            .subtitle(url)
            .text(ab) //ab contains a string instead of which we can retrieve the content of the url and store in it
            .buttons([
                builder.CardAction.openUrl(session, url, 'Click for more information') //url obtained from bing search.
            ]),
            new builder.HeroCard(session)
            .title('Check game requirements here')
            .subtitle(url1)
            .text(ab1) //ab contains a string instead of which we can retrieve the content of the url and store in it
            .buttons([
                builder.CardAction.openUrl(session, url1, 'Click for more information') //url obtained from bing search.
            ]),
            new builder.HeroCard(session)
            .title('Check game requirements here')
            .subtitle(url2)
            .text(ab2) //ab contains a string instead of which we can retrieve the content of the url and store in it
            .buttons([
                builder.CardAction.openUrl(session, url2, 'Click for more information') //url obtained from bing search.
            ])
        ]);
        session.sendTyping();
        session.send(msg);
    }
]);

//bing search   



lib.dialog('resolveIssue', [
        function (session) {
            pythontalking(session.conversationData.userInfo.completepath, function (message) {
                if (message == null) {
                    var str36 = 'That is the end of the Troubleshooting steps.';
                    return translate_lang(str36, session.conversationData.userInfo.language)
                        .then(function (str2_ans) {
                            session.send(str2_ans);

                            session.beginDialog('*:checklast');
                        });
                } else if (session.conversationData.userInfo.completepath.length == 4) {
                             session.beginDialog('*:checklast');
                } else {
                    session.conversationData.userInfo.latestnode = message;
                    session.conversationData.userInfo.completepath.push(message);
                    //session.send(session.conversationData.userInfo.completepath);
                    var filename = "./jsons/" + session.conversationData.userInfo.latestnode + ".json";
                    let f3 = fs.readFileSync(filename);
                    eval("var jsc = " + f3);
                    var key1 = "Steps_" + session.conversationData.userInfo.language.replace(/['"]+/g,  '');
                    session.conversationData.userInfo.datajson = jsc;
                    session.conversationData.userInfo.text = jsc[key1];


                    // getsteps(message, function (text) {
                    if (session.conversationData.userInfo.text == null) {
                        session.beginDialog('*:contact');
                    } else {
                        //   session.conversationData.userInfo.splittext = text.split('\n\n');
                        session.conversationData.userInfo.splittext = session.conversationData.userInfo.text;

                        // getcontents(message, function (content) {
                        console.log(session.conversationData.userInfo.text);
                        // session.conversationData.userInfo.content = content;
                        //  var len = session.conversationData.userInfo.splittext.length;
                        session.conversationData.userInfo.count = 0;
                        //builder.Prompts.text(session, "Are you ready?");
                        var str28 = 'Showing you the resolution steps for your issue, please perform these in the correct order.';
                        return translate_lang(str28, session.conversationData.userInfo.language)
                            .then(function (str2_ans) {
                                session.send(str2_ans);
                                var key2 = "Content_" + session.conversationData.userInfo.language.replace(/['"]+/g,  '');
                                session.send(jsc[key2]);
                                //session.send(session.conversationData.userInfo.content)
                                session.beginDialog('basic:displaysteps');
                                //  });
                            });

                    }

                    //});
                }
            });

        }
    ]).beginDialogAction('*:anger', '*:anger', {
        matches: 'anger'
    })
    .beginDialogAction('*:check', '*:check', {
        matches: 'handoff'
    })
    //.beginDialogAction('greetings', 'greetings', { matches : 'Greetings'})
    .beginDialogAction('*:qnad', '*:kbase', {
        matches: 'qna'
    })
    .beginDialogAction('*:showVideo', '*:showVideo', {
        matches: 'showVideo'
    })
     .beginDialogAction('*:service_center', '*:service_center', {
        matches: 'locationServiceCenter'
    });




   
lib.dialog('confirm', [
        function (session) {
            session.conversationData.userInfo.userresult = [];
            var userchoice = new Array("Yes", "No", "If you have performed the above steps, press Yes");
            async.mapSeries(userchoice, function (key1, callback2) {
                translate_lang(key1, session.conversationData.userInfo.language).then(function (response1) {
                    console.log("response from translate\n", response1);
                    session.conversationData.userInfo.userresult.push(response1);
                    // builder.Prompts.choice(session, 'Please select your elaborated problem?',  demoarray , { listStyle: builder.ListStyle.button,retryPrompt:"Please select your elaborated problem?" });
                    if (userchoice.indexOf(key1) == userchoice.length - 1) {
                        console.log('hip hip hurray');
                        builder.Prompts.choice(session, session.conversationData.userInfo.userresult[2], session.conversationData.userInfo.userresult.slice(0, 2), {
                            listStyle: builder.ListStyle.button,
                            retryPrompt: session.conversationData.userInfo.userresult[2]
                        });
                    }
                    callback2();
                });


            });
        },
        function (session, results) {
            var position1 = session.conversationData.userInfo.userresult.indexOf(results.response.entity);
            console.log('*************************', position1);
            if (position1 == 0) {
                session.beginDialog('basic:showsteps');
            } else {
                var str1111 = 'Ok, We\'ll resume once you complete the steps';

                translate_lang(str1111, session.conversationData.userInfo.language).then(function (response1) {
                    session.send(response1);
                    session.replaceDialog('confirm');

                });
            }

        }
    ]).beginDialogAction('*:anger', '*:anger', {
        matches: 'anger'
    })
    .beginDialogAction('*:check', '*:check', {
        matches: 'handoff'
    })
    //.beginDialogAction('greetings', 'greetings', { matches : 'Greetings'})
    .beginDialogAction('*:qnad', '*:kbase', {
        matches: 'qna'
    })
    .beginDialogAction('*:showVideo', '*:showVideo', {
        matches: 'showVideo'
    })
     .beginDialogAction('*:service_center', '*:service_center', {
        matches: 'locationServiceCenter'
    });


lib.dialog('showsteps', [
        function (session) {

            var i = session.conversationData.userInfo.splittext[session.conversationData.userInfo.count];
            i = i.toString();
            var r2 = i.match(/[hH][pP].dezide.com[^\s]+.jpg/g);
            i = i.replace(',', '');
            //count = count + 1;
            console.log(i);

            //var find1 = '/"';
            console.log(r2);
            if (r2 != null) {
                i = i.replace(/[hH][pP].dezide.com[^\s]+.jpg/g, '');
                i = i.replace(/\//g, '');
                console.log('In r2 ');
                r2.forEach(function (element) {
                    rnew = 'http:' + element;
                    console.log(rnew);
                    session.beginDialog('basic:loopImage');
                });

                console.log(".........................");
            }

            setTimeout(function () {
                session.sendTyping();
                session.send(i);

                session.conversationData.userInfo.count = session.conversationData.userInfo.count + 1;
                session.beginDialog('basic:displaysteps');
            }, 1200);

        }
    ]).beginDialogAction('*:anger', '*:anger', {
        matches: 'anger'
    })
    .beginDialogAction('*:check', '*:check', {
        matches: 'handoff'
    })
    //.beginDialogAction('greetings', 'greetings', { matches : 'Greetings'})
    .beginDialogAction('*:qnad', '*:kbase', {
        matches: 'qna'
    })
    .beginDialogAction('*:showVideo', '*:showVideo', {
        matches: 'showVideo'
    })
     .beginDialogAction('*:service_center', '*:service_center', {
        matches: 'locationServiceCenter'
    });


lib.dialog('secondstep', [
        function (session, results) {
            session.conversationData.userInfo.useroptions = [];
            session.conversationData.userInfo.finalstring = [];
            var str25 = "Choose one amongst the following";
            //builder.Prompts.text(session, "Enter No to continue");
            getedgelabels(session.conversationData.userInfo.latestnode, function (results) {
                unique(results);
                if (results.length == 0) {
                    session.beginDialog('*:contact');
                } else {
                    /* if (results.includes('No')) {
                         finalstring = 'Yes|';
                     }*/
                    if (results.includes('ways')) {
                        Array.prototype.remove = removeValue;
                        results.remove('ways');
                    }

                    console.log('results= ', results);
                    for (var j = 0; j <= results.length - 1; j++) {
                        
                        session.conversationData.userInfo.finalstring.push(results[j]);
                    }
                    setTimeout(function () {
                        return translate_lang(str25, session.conversationData.userInfo.language)
                            .then(function (str2_ans) {
                                builder.Prompts.choice(session, str2_ans, session.conversationData.userInfo.finalstring, {
                                    listStyle: builder.ListStyle.button,
                                    retryPrompt: str2_ans
                                });
                            });
                    }, 400);
                }
            });
        },
        function (session, results) {
            if (results.response.entity == 'Yes'||results.response.entity == 'Oui') {
                session.beginDialog('*:contact');
            } else {
                session.conversationData.userInfo.label = results.response.entity;
                options.args = [session.conversationData.userInfo.label];
                //session.send(session.conversationData.userInfo.label)
                console.log('options=  ', options);
                console.log('complete path ', session.conversationData.userInfo.completepath);
                session.beginDialog('basic:resolveIssue');
            }

        }
    ]).beginDialogAction('*:anger', '*:anger', {
        matches: 'anger'
    })
    .beginDialogAction('*:check', '*:check', {
        matches: 'handoff'
    })
    //.beginDialogAction('greetings', 'greetings', { matches : 'Greetings'})
    .beginDialogAction('*:qnad', '*:kbase', {
        matches: 'qna'
    })
    .beginDialogAction('*:showVideo', '*:showVideo', {
        matches: 'showVideo'
    })
     .beginDialogAction('*:service_center', '*:service_center', {
        matches: 'locationServiceCenter'
    });

lib.dialog('loopImage', [
        function (session) {
            var imagePattern2 = /\]/;
            // var x = imageText.split(imagePattern);
            var imageUrl = "https://"+rnew;

            var msg = new builder.Message(session)
                .attachments([{
                    contentType: "image/jpg",
                    contentUrl: rnew,
                }]);
            setTimeout(function () {
                session.endDialog(msg);
            }, 200);


        }
    ]).beginDialogAction('*:anger', '*:anger', {
        matches: 'anger'
    })
    .beginDialogAction('*:check', '*:check', {
        matches: 'handoff'
    })
    //.beginDialogAction('greetings', 'greetings', { matches : 'Greetings'})
    .beginDialogAction('*:qnad', '*:kbase', {
        matches: 'qna'
    })
    .beginDialogAction('*:showVideo', '*:showVideo', {
        matches: 'showVideo'
    })
     .beginDialogAction('*:service_center', '*:service_center', {
        matches: 'locationServiceCenter'
    });





lib.dialog('displaysteps', [
        function (session) {
            console.log('dialog stack *******',session.dialogStack());
            var len = session.conversationData.userInfo.splittext.length;
             console.log('@#$%^&', len);
            if (session.conversationData.userInfo.count >= len) {
                session.beginDialog('basic:secondstep');
            } else {
                // session.conversationData.userInfo.count = session.conversationData.userInfo.count+1;
                if (session.conversationData.userInfo.count % 6 == 0 && session.conversationData.userInfo.count != 0) {
                    session.beginDialog('basic:confirm');
                } else { //session.send(session.conversationData.userInfo.content);
                    session.beginDialog('basic:showsteps');
                }
            }

        }
    ]).beginDialogAction('*:anger', '*:anger', {
        matches: 'anger'
    })
    .beginDialogAction('*:check', '*:check', {
        matches: 'handoff'
    })
    //.beginDialogAction('greetings', 'greetings', { matches : 'Greetings'})
    .beginDialogAction('*:qnad', '*:kbase', {
        matches: 'qna'
    })
    .beginDialogAction('*:showVideo', '*:showVideo', {
        matches: 'showVideo'
    })
    .beginDialogAction('*:service_center', '*:service_center', {
        matches: 'locationServiceCenter'
    });




module.exports.createLibrary = function () {
    return lib.clone();
};