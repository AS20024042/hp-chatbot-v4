// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
//const { CancelAndHelpDialog } = require('./cancelAndHelpDialog');
//const { DateResolverDialog } = require('./dateResolverDialog');
const { UserProfile } = require("../userProfile");
const loop_image = "loop_image";
const CONFIRM_PROMPT = "confirmPrompt";
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
//const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const { DisplaySteps, display_steps } = require("./displaysteps.js");

class LoopImage extends ComponentDialog {
  constructor() {
    super(loop_image);

    // Define the main dialog and its related components.
    // this.addDialog(new DisplaySteps());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [this.evacuationPath.bind(this)])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }
  getInternetAttachment(url) {
    // NOTE: The contentUrl must be HTTPS.
    // return {
    //   name: "architecture-resize.png",
    //   contentType: "image/png",
    //   contentUrl: url
    // };
    var card = {
      type: "AdaptiveCard",
      version: "1.0",
      body: [
        {
          type: "Image",
          altText: "",
          url:
            "https://hp.dezide.com/data/pictures/hp-external/web_author/guid-4ded5cad-ffb6-4783-bdf9-3719ade50ed7-low_55920_en.jpg"
        }
      ],
      $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
    };

    return card;
  }

  async evacuationPath(stepContext) {
    console.log("Starting loopImage=====");
    var rnew = stepContext.options.rnew;

    console.log(rnew);

    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Image",
              altText: "",
              url: rnew
              // "https://hp.dezide.com/data/pictures/hp-external/web_author/guid-4ded5cad-ffb6-4783-bdf9-3719ade50ed7-low_55920_en.jpg"
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
   // return await stepContext.endDialog();
  }
 // return await stepContext.endDialog();
}

module.exports.LoopImage = LoopImage;
module.exports.loop_image = loop_image;
