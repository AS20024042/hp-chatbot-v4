// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");
const display_steps = "display_steps";
const CONFIRM_PROMPT = "confirmPrompt";
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
//const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const { ShowSteps, show_steps } = require("./showSteps.js");
const { ConfirmSteps, confirm_steps } = require("./confirmSteps.js");
const { SecondSteps, second_steps } = require("./secondSteps");

class DisplaySteps extends ComponentDialog {
  constructor() {
    super(display_steps);

    // Define the main dialog and its related components.
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new ConfirmSteps());
    this.addDialog(new ShowSteps());
    this.addDialog(new SecondSteps());
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [this.displayPath.bind(this)])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async displayPath(stepContext) {
    console.log("Starting display steps dialog");
    var dat = stepContext.options.dat;
    //var received_text = dat.splittext;
    var len = dat.splittext.length;
    var count = dat.count;
    if (count >= len) {
      console.log(
        "As count>=len, running second steps dialog count = " +
          count +
          "len= " +
          len
      );
      return await stepContext.replaceDialog(second_steps, {
        dat: dat
      });
    } else {
      if (count % 6 == 0 && count != 0) {
        console.log("Begin Confirm Dialog");
        return await stepContext.beginDialog("confirm_steps", {
          dat: dat
        });
        // return await stepContext.endDialog();
        //  await stepContext.beginDialog("confirm_steps");
      } else {
        return await stepContext.beginDialog("show_steps", {
          dat: dat
        });
      }
    }
  }
}

module.exports.DisplaySteps = DisplaySteps;
module.exports.display_steps = display_steps;
