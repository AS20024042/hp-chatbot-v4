var PythonShell = require("python-shell");
const py = function nextNode(dat, arg) {
  var replyback;
  return new Promise((resolve, reject) => {
    console.log("went inside python page");
    var options = {
      mode: "text",
      pythonPath: "bots/hey/py/python364x64/python",
      pythonOptions: ["-u"], // get print results in real-time
      args: [arg]
    };
    var pyshell = new PythonShell(
      "update_weight/project_traversing.py",
      options
    );
    var bbb = JSON.stringify(dat.completepath);
    console.log(bbb + "   ++");
    pyshell.send(bbb);
    pyshell.on("message", function(message) {
      // received a message sent from the Python script (a simple "print" statement)
      //console.log(message);
      message = message.replace("\r", "");
      replyback = message;
      console.log("this is msg " + replyback);
    });
    pyshell.end(function(err) {
      if (err) {
        // throw err;
        reject(err);
      } else {
        console.log("finished");
        resolve(replyback);
      }
    });
  });
};
module.exports.py = py;
