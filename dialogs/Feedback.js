// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { InputHints, MessageFactory, CardFactory } = require("botbuilder");

// const { ChoiceFactory } = require('botbuilder-choices');

const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog,
  ComponentDialog,
  ChoicePrompt
} = require("botbuilder-dialogs");

const { CancelAndHelpDialog } = require("./cancelAndHelpDialog");
const feedback = "feedback";

const feed = require("../cards/testfeedback.json");

const CONFIRM_PROMPT = "confirmPrompt";
const CHOICE_PROMPT = "choicePrompt";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG = "waterfallDialog";

var res, msg;

class Feedback extends ComponentDialog {
  constructor() {
    super(feedback);

    this.addDialog(new TextPrompt(TEXT_PROMPT))
      .addDialog(new ConfirmPrompt(CONFIRM_PROMPT))
      .addDialog(new ChoicePrompt(CHOICE_PROMPT))
      .addDialog(
        new WaterfallDialog(WATERFALL_DIALOG, [
          this.service.bind(this),
          this.description.bind(this),
          this.feedthird.bind(this)
        ])
      );

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async service(stepContext) {
    console.log("feedfirst");

    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: "Please provide your feedback:"
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: "👍",
                      data: "good"
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: "👎",
                  data: "bad"
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    return await stepContext.prompt("textPrompt", {});
  }

  async description(stepContext) {
    console.log("feed second");
    res = stepContext.result;

    if (res == "good") {
      msg = "What did you like about our bot?";
    } else {
      msg = "What didn't you like about our bot?";
    }

    // await stepContext.context.sendActivity({
    //   text: "",
    //   attachments: [
    //     CardFactory.adaptiveCard({
    //       type: "AdaptiveCard",
    //       version: "1.0",
    //       body: [
    //         {
    //           type: "Container",
    //           items: [
    //             {
    //               type: "TextBlock",
    //               text: msg
    //             }
    //           ]
    //         }
    //       ],
    //       $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
    //     })
    //   ]
    // });
    await stepContext.context.sendActivity(msg);
    return await stepContext.prompt("textPrompt", {});
  }

  async feedthird(stepContext) {
    await stepContext.context.sendActivity(
      "Your feed back is very much appriciated!!"
    );
    return await stepContext.endDialog();
  }
}
module.exports.Feedback = Feedback;
module.exports.feedback = feedback;
