const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
//const issue_choice = require("../cards/issue_choice.json");
const { gamecard } = require("../cards/gameper");
const fs = require("fs");
const Bing_Search = "Bing_Search";
const TEXT_PROMPT = "TEXT_PROMPT";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const { DisplaySteps, display_steps } = require("./displaysteps.js");
var issueMapping = new Array();
const { UserProfile } = require("../userProfile");
var subscriptionKey = process.env.searchkey;
var customConfigId = process.env.customConfigid;
var request = require("request");
const https = require("https");
var searchTerm;
//const SUBSCRIPTION_KEY = "99fb286e00184843b45f1e0bc43d9ac4";
const search = function bingWebSearch(query) {
  return new Promise((resolve, reject) => {
    var finalsearch;
    console.log("entered the bing");
    const SUBSCRIPTION_KEY = "99fb286e00184843b45f1e0bc43d9ac4";
    https.get(
      {
        hostname: "api.cognitive.microsoft.com",
        path: "/bing/v7.0/search?q=" + encodeURIComponent(query),
        headers: { "Ocp-Apim-Subscription-Key": SUBSCRIPTION_KEY }
      },
      res => {
        console.log("entered the res");
        let body = "";
        res.on("data", part => (body += part));
        res.on("end", () => {
          for (var header in res.headers) {
            if (
              header.startsWith("bingapis-") ||
              header.startsWith("x-msedge-")
            ) {
              console.log(header + ": " + res.headers[header]);
            }
          }
          console.log("\nJSON Response:\n");
          //console.dir(JSON.parse(body), { colors: false, depth: null });
          var fin = JSON.parse(body);
          console.log("======" + fin);
          console.log(fin.webPages.value[0].url);
          console.log(fin.webPages.value[0].snippet);
          finalsearch =
            "Bing Search Result: " +
            fin.webPages.value[0].url +
            "   Snippet:   " +
            fin.webPages.value[0].snippet;
          console.log(finalsearch);
          resolve(finalsearch);
        });
        res.on("error", e => {
          console.log("Error: " + e.message);
          throw e;
        });
        // var fin = JSON.parse(body);
        // //var fin = body;

        // console.log("======" + fin);
        // console.log(fin.webPages.value[0].url);
        // console.log(fin.webPages.value[0].snippet);
        // finalsearch =
        //   "this is bing search: " +
        //   fin.webPages.value[0].url +
        //   " ====" +
        //   fin.webPages.value[0].snippet;
        // console.log(finalsearch);
        // resolve("finalsearch");
      }
    );
  });
};

module.exports.search = search;
