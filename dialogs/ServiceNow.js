// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");
const service_now = "service_now";
const CONFIRM_PROMPT = "confirmPrompt";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const { LoopImage, loop_image } = require("./loopImage.js");
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const ticket_options = require("../cards/serviceNow.json");
const TEXT_PROMPT = "TEXT_PROMPT";
var dat;
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var btoa = require("btoa");
const sn = require("servicenow-rest-api");
//var document = require("document");

class ServiceNow extends ComponentDialog {
  constructor() {
    super(service_now);
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(new LoopImage());
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.showPath.bind(this),
        this.raiseStep.bind(this)
      ])
    );
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async showPath(stepContext) {
    console.log("--Starting Service Now Dialog--");

    dat = stepContext.options.dat;

    // var requestBody = '{"u_issue":dat.main}';

    // var client = new XMLHttpRequest();
    // client.open(
    //   "post",
    //   "https://dev73810.service-now.com/api/now/table/u_hpbot"
    // );

    // client.setRequestHeader("Accept", "application/json");
    // client.setRequestHeader("Content-Type", "application/json");

    // //Eg. UserName="admin", Password="admin" for this code sample.
    // client.setRequestHeader(
    //   "Authorization",
    //   "Basic " + btoa("admin" + ":" + "Ash@top123")
    // );

    // client.onreadystatechange = function() {
    //   if (this.readyState == this.DONE) {
    //      .getElementById("response").innerHTML =
    //       this.status + this.response;
    //   }
    // };
    // client.send(requestBody);

    // const options = MessageFactory.attachment(
    //   CardFactory.adaptiveCard(ticket_options)
    // );
    // return await stepContext.prompt(TEXT_PROMPT, {
    //   prompt: options
    // });

    // const ServiceNow = new sn(
    //   "https://dev73810.service-now.com/",
    //   "admin",
    //   "Ash@top123"
    // );

    // ServiceNow.Authenticate(res => {
    //   console.log(res.status);
    // });
    // ServiceNow.getSampleData("u_hpbot", res => {
    //   //
    //   console.log(res.status);
    // });

    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "TextBlock",
              text: "Select the priority of your issue:"
            },
            {
              type: "Container",
              items: [
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: "Critical",
                      data: "Critical"
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: "Moderate",
                      data: "Moderate"
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: "low",
                      data: "Low"
                    }
                  ]
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    console.log("displayed card");

    return await stepContext.prompt("textPrompt", {});
  }

  async raiseStep(stepContext) {
    console.log("--In raise step--");
    var issue = stepContext.result;
    console.log(issue);
    return await stepContext.context.sendActivity("Service Now Ticket Created Successfully with ticket no: HP167402. Please save it for future reference!!");
    
    // if (issue == "Critical") {
    //   await stepContext.context.sendActivity("critical ticket created");
    // } else if (issue == "Moderate") {
    //   await stepContext.context.sendActivity("moderate ticket created");
    // } else if (issue == "Low") {
    //   await stepContext.context.sendActivity("low ticket created");
    // }

   // return await stepContext.endDialog();
  }
}

module.exports.ServiceNow = ServiceNow;
module.exports.service_now = service_now;
