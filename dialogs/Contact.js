// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");

const CONFIRM_PROMPT = "confirmPrompt";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
const TEXT_PROMPT = "textPrompt";
const { Feedback, feedback } = require("./Feedback");
const { ServiceNow, service_now } = require("./ServiceNow");

const contact = "contact";
const feedbackcard = require("../cards/feedbackcard.json");
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
//const { DisplaySteps, display_steps } = require("./displaysteps.js");
var dat;
var a, b, c, d, aa, bb, cc, dd, msg;
var solve_yes1, solve_yes2, solve_no1, solve_no2;

class Contact extends ComponentDialog {
  constructor() {
    super(contact);

    // Define the main dialog and its related components.
    // this.addDialog(new DisplaySteps());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new Feedback());
    this.addDialog(new ServiceNow());
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.feed_first.bind(this),
        this.feed_second.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async feed_first(stepContext) {
    console.log("contact first");
    dat = stepContext.options.dat;
    // let options = MessageFactory.attachment(
    //   CardFactory.adaptiveCard(contactcard)
    // );
    // return await stepContext.prompt(TEXT_PROMPT, {
    //   prompt: options
    // });
    //return await stepContext.context.sendActivity("contact achieved");
    msg = "Please choose one of the following";
    a = aa = "Connect to an expert";
    b = bb = "Service now ticket";
    c = cc = "Give feedback";
    if (dat.lang != "en") {
      await Translate.translate(msg, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg = value;
      });
      await Translate.translate(a, dat.lang).then(function(value) {
        console.log("converterd " + value);
        a = value;
      });
      await Translate.translate(b, dat.lang).then(function(value) {
        console.log("converterd " + value);
        b = value;
      });
      await Translate.translate(c, dat.lang).then(function(value) {
        console.log("converterd " + value);
        c = value;
      });
    }
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: a,
                      data: "agent"
                    }
                  ]
                },
                // {
                //   type: "ActionSet",
                //   actions: [
                //     {
                //       type: "Action.Submit",
                //       title: b,
                //       data: "create the service now ticket"
                //     }
                //   ]
                // },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: c,
                      data: "feedback"
                    }
                  ]
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });

    return await stepContext.prompt("textPrompt", {});
  }
  async feed_second(stepContext) {
    // var a = stepContext.context.activity;
    // console.log(a);
    dat.contact = stepContext.result;
    // if (dat.lang != "en") {
    //   if (stepContext.result == a) {
    //     dat.contact = aa;
    //   } else if (stepContext.result == b) {
    //     dat.contact = bb;
    //   } else if (stepContext.result == c) {
    //     dat.contact = cc;
    //   }
    // } else {
    //   dat.contact = stepContext.result;
    // }s
    console.log("enter contact second");
    console.log(dat.contact);
    if (dat.contact == "feedback") {
      return await stepContext.beginDialog(feedback, {
        dat: dat
      });
    } else if (dat.contact == "create the service now ticket") {
      return await stepContext.beginDialog(service_now, {
        dat: dat
      });
    } else {
      dat.main = dat.contact;
      console.log("================");
      console.log("from Contact");
      console.log("================");
      return await stepContext.replaceDialog("Main_Dialog2", {
        dat: dat
      });
    }
  }
}

module.exports.Contact = Contact;
module.exports.contact = contact;
