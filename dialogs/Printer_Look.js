const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { gamecard } = require("../cards/gameper");
const fs = require("fs");
const Performance_Dialog = "Performance_Dialog";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const Search = require("./bingsearch");
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { Feedback, feedback } = require("./Feedback");
const { Contact, contact } = require("./Contact.js");
var dat;
const Printer_Issue = "Printer_Issue";
const Printer_Wifi_Issue = "Printer_Wifi_Issue";
const Printer_Offline_Issue = "Printer_Offline_Issue";
const Printer_Look_Issue = "Printer_Look_Issue";
const { MainDialog2, Main_Dialog2 } = require("./mainDialog2");
var a, b, c, d, aa, bb, cc, dd, msg;
var solve_yes1, solve_yes2, solve_no1, solve_no2;

class Printer_Look_Dialog extends ComponentDialog {
  constructor(conversationState, userState) {
    super(Printer_Look_Issue);

    // The state management objects for the conversation and user state.
    this.conversationState = conversationState;
    this.userState = userState;

    // Define the main dialog and its related components.

    this.addDialog(new Feedback());
    this.addDialog(new Contact());
    this.addDialog(new DisplaySteps());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.lastStep.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async initialStep(stepContext) {
    dat = stepContext.options.dat;
    console.log("printer dosent look good");
    var fil = "20";

    var filename = "./printerjson/" + fil + ".json";
    let f2 = fs.readFileSync(filename);
    var jsc;
    eval("jsc = " + f2);
    var key = "Solve";
    var ab = jsc[key];
    var len = ab.length;
    for (var q = 0; q < len; q++) {
      var lan = ab[q];
      if (dat.lang != "en") {
        await Translate.translate(lan, dat.lang).then(function(value) {
          console.log("converterd " + value);
          lan = value;
        });
      }

      await stepContext.context.sendActivity(lan);
    }
    var msg_solve = "Did it solve the problem";

    solve_no1 = solve_no2 = "No";
    solve_yes1 = solve_yes2 = "Yes";
    if (dat.lang != "en") {
      await Translate.translate(msg_solve, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg_solve = value;
      });
      await Translate.translate(solve_no1, dat.lang).then(function(value) {
        console.log("converterd " + value);
        solve_no1 = value;
      });
      await Translate.translate(solve_yes1, dat.lang).then(function(value) {
        console.log("converterd " + value);
        solve_yes1 = value;
      });
    }
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg_solve
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: solve_yes1,
                      data: solve_yes1
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: solve_no1,
                  data: solve_no1
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    return await stepContext.prompt("textPrompt", {});
  }
  async lastStep(stepContext) {
    var ans = stepContext.result;
    console.log(ans);
    if (ans == solve_yes1) {
      ans = "Yes";
    } else if (ans == solve_no1) {
      ans = "No";
    }
    if (ans == "Yes") {
      await stepContext.context.sendActivity("Now entering feedback dialog");
      return await stepContext.beginDialog(feedback, {
        dat: dat
      });
    } else if (ans == "No") {
      await stepContext.context.sendActivity("Now entering contact dialog");
      return await stepContext.beginDialog(contact, {
        dat: dat
      });
    } else {
      dat.main = ans;
      return await stepContext.replaceDialog("Main_Dialog2", {
        dat: dat
      });
    }
  }
}
module.exports.Printer_Look_Dialog = Printer_Look_Dialog;
module.exports.Printer_Look_Issue = Printer_Look_Issue;
