// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");
const show_steps = "show_steps";
const CONFIRM_PROMPT = "confirmPrompt";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const { LoopImage, loop_image } = require("./loopImage.js");
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
//const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const { DisplaySteps, display_steps } = require("./displaysteps.js");

class ShowSteps extends ComponentDialog {
  constructor() {
    super(show_steps);

    // Define the main dialog and its related components.
    // this.addDialog(new DisplaySteps());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(new LoopImage());
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [this.showPath.bind(this)])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async showPath(stepContext) {
    console.log("--Starting Show steps dialog--");
    var dat = stepContext.options.dat;
    var i = dat.splittext[dat.count];
    i = i.toString();
    var r2 = i.match(/[hH][pP].dezide.com[^\s]+.jpg/g);
    i = i.replace(",", "");
    //     var message = activity.CreateReply("");
    // message.Type = "message";
    // message.Attachments = new List<Attachment>();
    if (dat.lang != "en") {
      await Translate.translate(i, dat.lang).then(function(value) {
        console.log("converterd " + value);
        i = value;
      });
    }
    console.log(dat.count + " ~~~~~~~ " + i + " ~~~~~~~~");
    //start working on image loop here

    if (r2 != null) {
      console.log(r2);
      i = i.replace(/[hH][pP].dezide.com[^\s]+.jpg/g, "");
      i = i.replace(/\//g, "");
      console.log("In r2 ");
      r2.forEach(function(element) {
        var rnew = "https:" + element;

        var rnew = "https://" + element;
        console.log("+++++++");
        console.log(rnew);
        console.log("+++++++");
        //console.log(rnew);
        //const reply = { type: ActivityTypes.Message };
        //reply.text = "This is an internet attachment.";
        //reply.attachments = [this.getInternetAttachment(rnew)];
        // turnContext.sendActivity(reply);
        //await stepContext.context.sendActivity(reply);
        console.log("Starting loopImage===== of showsteps");
        console.log(typeof rnew);
        stepContext.beginDialog(loop_image, { rnew: rnew });

        //var rnew = stepContext.options.rnew;
        //var imageUrl = "https://" + rnew;

        //message.Attachments.Add(new Attachment { ContentUrl = rnew, ContentType = "image/png" });
      });

      console.log(".........................");
    }

    //await stepContext.context.sendActivity(msg);
    await stepContext.context.sendActivity(i);
    dat.count = dat.count + 1;
    return await stepContext.beginDialog("display_steps", {
      dat: dat
    });
  }
  getInternetAttachment(rnew) {
    // NOTE: The contentUrl must be HTTPS.
    return {
      name: "architecture-resize.png",
      contentType: "image/png",
      height: "100",
      contentUrl: rnew
    };
  }
//  return await stepContext.endDialog();
}

module.exports.ShowSteps = ShowSteps;
module.exports.show_steps = show_steps;
