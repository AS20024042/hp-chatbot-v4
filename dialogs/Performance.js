const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { gamecard } = require("../cards/gameper");
const fs = require("fs");
const Performance_Dialog = "Performance_Dialog";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const Search = require("./bingsearch");
const { DisplaySteps, display_steps } = require("./displaysteps.js");

const { Feedback, feedback } = require("./Feedback");
const { Contact, contact } = require("./Contact.js");
var issueMapping = new Array();
issueMapping["no Boot"] = "";
issueMapping["Performance application"] = "";
issueMapping["Performance General"] = "";
issueMapping["Performance_Games"] = "Performance issue with games";
issueMapping["Update"] = "";
issueMapping["Performance Start"] = "Windows Start";
const { UserProfile } = require("../userProfile");
var cho = new Array();
cho["Check system requirement"] = "2C22085";
cho["Computer over heating"] = "0C22096.C21299";
cho["Update drivers"] = "1C22096.C18172";
cho["Not Sure!"] = "2C22085";
cho["Do you have many application in your system?"] = "1C22099.C18187";
cho["Perform hard reset"] = "1C22099.C18264";
cho["Update drivers!"] = "1C22099.C18263";
cho["Not Sure"] = "5C22085";

var chonew1 = new Array(
  "2C22085",
  "0C22096.C21299",
  "1C22096.C18172",
  "2C22085"
);

var demoarray1 = new Array(
  "System requirements",
  "Laptop Heating",
  "Drivers",
  "Not Sure"
);
var chonew = [];
var user_choice = "";
var dat;
var crap;
var val1, val2, val3, val4, val11, val22, val33, val44;

class PerformanceDialog extends ComponentDialog {
  constructor(conversationState, userState) {
    super(Performance_Dialog);

    // The state management objects for the conversation and user state.
    this.conversationState = conversationState;
    this.userState = userState;

    // Define the main dialog and its related components.

    this.addDialog(new Feedback());
    this.addDialog(new Contact());
    this.addDialog(new DisplaySteps());

    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.performanceStep.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async initialStep(stepContext) {
    console.log("Performance games inital step");
    chonew = chonew1;
    dat = stepContext.options.dat;
    var st = "What is the issue?";
    val1 = val11 = "System requirements";
    val2 = val22 = "Laptop Heating";
    val3 = val33 = "Drivers";
    val4 = val44 = "Not sure";
    if (dat.lang != "en") {
      await Translate.translate(st, dat.lang).then(function(value) {
        console.log("converterd " + value);
        st = value;
      });
      await Translate.translate(val1, dat.lang).then(function(value) {
        console.log("converterd " + value);
        val1 = value;
      });
      await Translate.translate(val2, dat.lang).then(function(value) {
        console.log("converterd " + value);
        val2 = value;
      });
      await Translate.translate(val3, dat.lang).then(function(value) {
        console.log("converterd " + value);
        val3 = value;
      });
      await Translate.translate(val4, dat.lang).then(function(value) {
        console.log("converterd " + value);
        val4 = value;
      });
    }

    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: st
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: val1,
                      data: val1
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: val2,
                      data: val2
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: val3,
                      data: val3
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: val4,
                      data: val4
                    }
                  ]
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    return await stepContext.prompt("textPrompt", {});
  }
  async performanceStep(stepContext) {
    dat.choice = stepContext.result;

    console.log(dat.choice + "  00000000000000");
    if (dat.choice == val1) {
      dat.choice = val11;
      dat.issue = "System requirements";
    } else if (dat.choice == val2) {
      dat.choice = val22;
      dat.issue = "Laptop Heating";
    } else if (dat.choice == val3) {
      dat.choice = val33;
      dat.issue = "drivers";
    } else if (dat.choice == val4) {
      dat.choice = val44;
      dat.issue = "Not sure";
    } else if (dat.choice == "contact") {
      return await stepContext.replaceDialog(contact, {
        dat: dat
      });
    } else {
      console.log("================");
      console.log("from Performance==");
      console.log("================");
      dat.main = dat.choice;
      return await stepContext.replaceDialog("Main_Dialog2", {
        dat: dat
      });
    }

    //dat.issue = stepContext.result.value.data;
    console.log("++++++++++" + dat.issue);
    var position = demoarray1.indexOf(dat.choice);
    // making the DAT file;
    //dat.cho = chonew;
    //dat.postition = position;
    dat.latestnode = chonew[position];
    dat.completepath.push(dat.latestnode);
    var filename = "./jsons/" + dat.latestnode + ".json";
    let f2 = fs.readFileSync(filename);
    var jsc;
    eval("jsc = " + f2);
    var key1 = "Steps_en";
    dat.datajson = jsc;
    dat.text = jsc[key1];
    var bing = "Bing search results";
    if (dat.lang != "en") {
      await Translate.translate(bing, dat.lang).then(function(value) {
        console.log("converterd " + value);
        bing = value;
      });
    }

    if (dat.text == null) {
      await stepContext.context.sendActivity("Run Contact Dialog");
    } else {
      if (dat.issue != null) {
      //  await stepContext.context.sendActivity(bing);
        await Search.search(dat.issue).then(function(value) {
          crap = value;
        });
        await Translate.translate(crap, dat.lang).then(function(value) {
          console.log("converterd " + value);
          crap = value;
        });
        await stepContext.context.sendActivity(crap);
      }
      dat.splittext = dat.text;
      dat.count = 0;
      var str28 =
        "Trouble-Shooting Steps:";
      var key2 = "Content_en";
      var intro = jsc[key2];
      if (dat.lang != "en") {
        await Translate.translate(intro, dat.lang).then(function(value) {
          console.log("converterd " + value);
          intro = value;
        });
      }
      if (dat.lang != "en") {
        await Translate.translate(str28, dat.lang).then(function(value) {
          console.log("converterd " + value);
          str28 = value;
        });
      }
      await stepContext.context.sendActivity(str28);
      await stepContext.context.sendActivity(intro);
      return await stepContext.replaceDialog(display_steps, {
        dat: dat
      });
    }

    //return await stepContext.endDialog();
  }
}

module.exports.PerformanceDialog = PerformanceDialog;
module.exports.Performance_Dialog = Performance_Dialog;
