const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { gamecard } = require("../cards/gameper");
const fs = require("fs");
const Performance_Dialog = "Performance_Dialog";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const Search = require("./bingsearch");
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { Feedback, feedback } = require("./Feedback");
const { Contact, contact } = require("./Contact.js");
var dat;
const Printer_Issue = "Printer_Issue";
const Printer_Wifi_Issue = "Printer_Wifi_Issue";
const Printer_Offline_Issue = "Printer_Offline_Issue";
const Printer_Quality_Issue = "Printer_Quality_Issue";
const { MainDialog2, Main_Dialog2 } = require("./mainDialog2");
var a, b, c, d, aa, bb, cc, dd, msg, ans;
var solve_yes1, solve_yes2, solve_no1, solve_no2;

class Printer_Quality_Dialog extends ComponentDialog {
  constructor(conversationState, userState) {
    super(Printer_Quality_Issue);

    // The state management objects for the conversation and user state.
    this.conversationState = conversationState;
    this.userState = userState;

    // Define the main dialog and its related components.

    this.addDialog(new Feedback());
    this.addDialog(new Contact());
    this.addDialog(new DisplaySteps());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.midStep.bind(this),
        this.aStep.bind(this),
        this.bStep.bind(this),
        this.cStep.bind(this),
        this.dStep.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }
  async initialStep(stepContext) {
    dat = stepContext.options.dat;
    await stepContext.context.sendActivity(
      "Have you tried basic troubleshooting steps such as giving the printer a power reset?"
    );
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                // {
                //   type: "TextBlock",
                //   text:
                //     "Have you tried basic troubleshooting steps such as giving the printer a power reset?"
                // },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: "Yes, but it dosent solve the issue",
                      data: "Yes"
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: "No",
                  data: "No"
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    return await stepContext.prompt("textPrompt", {});
  }
  async midStep(stepContext) {
    console.log("midstep");
    var ans = stepContext.result;
    if (ans == "Yes") {
      return await stepContext.next();
    } else if (ans == "No") {
      await stepContext.context.sendActivity(
        "Please try troubleshooting first and check!!"
      );

      return await stepContext.endDialog();
    } else {
      dat.main = ans;
      return await stepContext.replaceDialog(Main_Dialog2, {
        dat: dat
      });
    }
  }
  async aStep(stepContext) {
    console.log("astep");
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text:
                    "Is the printer able to make a good copy in stand-alone mode?"
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: "Yes",
                      data: "Yes"
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: "No",
                  data: "No"
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    return await stepContext.prompt("textPrompt", {});
  }
  async bStep(stepContext) {
    console.log("bstep");
    ans = stepContext.result;
    if (ans == "Yes") {
      await stepContext.context.sendActivity(
        "If the copy works fine, it means that the printer hardware is fully functional. It could be a network or software related issue."
      );
      await stepContext.context.sendActivity({
        text: "",
        attachments: [
          CardFactory.adaptiveCard({
            type: "AdaptiveCard",
            version: "1.0",
            body: [
              {
                type: "Container",
                items: [
                  {
                    type: "TextBlock",
                    text:
                      "Please confirm the Operating System of the device from where you’re printing."
                  },
                  {
                    type: "ActionSet",
                    actions: [
                      {
                        type: "Action.Submit",
                        title: "Windows",
                        data: "Windows"
                      }
                    ]
                  }
                ]
              },
              {
                type: "ActionSet",
                actions: [
                  {
                    type: "Action.Submit",
                    title: "Mac",
                    data: "Mac"
                  }
                ]
              }
            ],
            $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
          })
        ]
      });
      return await stepContext.prompt("textPrompt", {});
    } else if (ans == "No") {
      var fil = "60";

      var filename = "./printerjson/" + fil + ".json";
      let f2 = fs.readFileSync(filename);
      var jsc;
      eval("jsc = " + f2);
      var key = "Solve";
      var ab = jsc[key];
      var len = ab.length;
      for (var q = 0; q < len; q++) {
        var lan = ab[q];
        if (dat.lang != "en") {
          await Translate.translate(lan, dat.lang).then(function(value) {
            console.log("converterd " + value);
            lan = value;
          });
        }

        await stepContext.context.sendActivity(lan);
      }
      await stepContext.context.sendActivity({
        text: "",
        attachments: [
          CardFactory.adaptiveCard({
            type: "AdaptiveCard",
            version: "1.0",
            body: [
              {
                type: "Container",
                items: [
                  {
                    type: "TextBlock",
                    text: "Did your problem get solved now?"
                  },
                  {
                    type: "ActionSet",
                    actions: [
                      {
                        type: "Action.Submit",
                        title: "Yes",
                        data: "Yes"
                      }
                    ]
                  }
                ]
              },
              {
                type: "ActionSet",
                actions: [
                  {
                    type: "Action.Submit",
                    title: "No",
                    data: "No"
                  }
                ]
              }
            ],
            $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
          })
        ]
      });
      return await stepContext.prompt("textPrompt", {});
    } else {
      dat.main = ans;
      return await stepContext.replaceDialog(Main_Dialog2, {
        dat: dat
      });
    }
  }
  async cStep(stepContext) {
    console.log(stepContext.result);
    if (ans == "Yes") {
      await stepContext.context.sendActivity(
        "Thank you for the information. I have logged a ticket with Case Number: HP210047 so that you can be connected to an HP Technician. A typical session may take approximately 20-30 minutes. To continue the chat enter your next query or Call the Voice Support at 1800-474-6836."
      );

      return await stepContext.prompt("textPrompt", {});
    } else if (ans == "No") {
      if (stepContext.result == "No") {
        await stepContext.context.sendActivity(
          "As you are still facing the issue, I have logged a ticket with Case Number: HP478376 so that you can be connected to an HP Technician. A typical session may take approximately 20-30 minutes. To continue the chat enter your next query or Call the Voice Support at 1800-474-6836."
        );
        return await stepContext.replaceDialog(contact, {
          dat: dat
        });
        //return await stepContext.prompt("textPrompt", {});
        //return await stepContext.endDialog();
      } else {
        await stepContext.context.sendActivity(
          "Okay,Thanks for using this Bot"
        );
        return await stepContext.replaceDialog(feedback, {
          dat: dat
        });

        //return await stepContext.prompt("textPrompt", {});
        //return await stepContext.endDialog();
      }
    }
  }

  async dStep(stepContext) {
    console.log(stepContext.result);
    dat.lang = "en";
    dat.main = "Printer issue";
    return await stepContext.replaceDialog(Main_Dialog2, {
      dat: dat
    });
  }
}
module.exports.Printer_Quality_Dialog = Printer_Quality_Dialog;
module.exports.Printer_Quality_Issue = Printer_Quality_Issue;
