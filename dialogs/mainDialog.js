// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory
} = require("botbuilder");
const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus,
  WaterfallDialog
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");
const { TextPrompt } = require("botbuilder-dialogs");
const { LuisHelper } = require("./luisHelper");
const { PerformanceDialog, Performance_Dialog } = require("./Performance.js");
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { ConfirmSteps, confirm_steps } = require("./confirmSteps.js");
const { PrinterDialog, Printer_Issue } = require("./Printer.js");
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const { Contact, contact } = require("./Contact.js");
const { Feedback, feedback } = require("./Feedback");
const main_dialog = "main_dialog";
const TEXT_PROMPT = "textPrompt";
const { MainDialog2, Main_Dialog2 } = require("./mainDialog2");
const {
  Printer_Wifi_Dialog,
  Printer_Wifi_Issue
} = require("./Printer_Wifi_Issue");

const {
  Printer_Job_Stuck_Issue,
  Printer_Job_Stuck_Dialog
} = require("./Printer_Job_Stuck.js");
const {
  Printer_Offline_Issue,
  Printer_Offline_Dialog
} = require("./Printer_Offline.js");
const {
  Printer_Look_Dialog,
  Printer_Look_Issue
} = require("./Printer_Look.js");
const {
  Printer_Quality_Dialog,
  Printer_Quality_Issue
} = require("./Printer_Quality.js");
var dat;
var a, b, c, d, e, f, g, ee, ff, gg, aa, bb, cc, dd, msg;

class MainDialog extends ComponentDialog {
  constructor() {
    super(main_dialog);
    // Define the main dialog and its related components.
    // This is a sample "book a flight" dialog.
    this.addDialog(new PerformanceDialog());
    this.addDialog(new DisplaySteps());
    this.addDialog(new PrinterDialog());
    this.addDialog(new Printer_Wifi_Dialog());
    this.addDialog(new Printer_Job_Stuck_Dialog());
    this.addDialog(new Printer_Offline_Dialog());
    this.addDialog(new Printer_Look_Dialog());
    this.addDialog(new Printer_Quality_Dialog());
    this.addDialog(new ConfirmSteps());
    this.addDialog(new Contact());
    this.addDialog(new MainDialog2());
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(new Feedback());
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.luisStep.bind(this),

        this.printername.bind(this),
        this.performanceStep.bind(this),
        this.lastStep.bind(this)
        //this.finalStep.bind(this)
      ])
    );
    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
   * If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  /**
   * First step in the waterfall dialog. Prompts the user for a command.
   * Currently, this expects a booking request, like "book me a flight from Paris to Berlin on march 22"
   * Note that the sample LUIS model will only recognize Paris, Berlin, New York and London as airport cities.
   */
  /**
   * 1. Prompts the user if the user is not in the middle of a dialog.
   * 2. Re-prompts the user when an invalid input is received.
   *
   * @param {WaterfallStepContext} stepContext
   */

  async luisStep(stepContext) {
    //Connect with mongoDB and send the card mentioning user details like name, device information etc.
    if (
      !process.env.LuisAppId ||
      !process.env.LuisAPIKey ||
      !process.env.LuisAPIHostName
    ) {
      await stepContext.context.sendActivity(
        "NOTE: LUIS is not configured. To enable all capabilities, add `LuisAppId`, `LuisAPIKey` and `LuisAPIHostName` to the .env file."
      );
    }
    return await stepContext.next();

    // return await stepContext.prompt('Text_Prompt', { prompt: 'Please describe the problem you are currently experiencing.' });
  }

  async printername(stepContext) {
    dat = new UserProfile();
    dat.wrong = 0;
    dat.name = stepContext.context.activity.text;
    console.log("User name===", dat.name);

    //await stepContext.context.sendActivity("enter the querry");
    msg = "Hi " + dat.name + ", How can I assist you today?";
    a = aa = "Printer Issues";
    b = bb = "Laptop performance";
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: a,
                      data: a
                     // data :{
                      //  value: a
                     // }
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: b,
                  data: b
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });

    return await stepContext.prompt("textPrompt", {});
  }
  async performanceStep(stepContext) {
   // console.log(stepContext.context.activity.value);
    console.log(stepContext.result + "main issue");
    dat.lang = "en";
    if (dat.lang != "en") {
      if (stepContext.result == a) {
        dat.issue = aa;
      } else if (stepContext.result == b) {
        dat.issue = bb;
      } else if (stepContext.result == c) {
        dat.issue = cc;
      } else if (stepContext.result == d) {
        dat.issue = dd;
      } else {
        dat.issue = stepContext.result;
      }
    } else {
      dat.issue = stepContext.result;
      //dat.issue=stepContext.context.activity.value.data
    }

    console.log("is main issue is == " + dat.issue);

    if (dat.issue == bb) {
      dat.completepath.push("Performance issue with games");
      return await stepContext.replaceDialog(Performance_Dialog, {
        dat: dat
      });
      //return await stepContext.endDialog();
    } else if (dat.issue == aa) {
      return await stepContext.replaceDialog(Printer_Issue, {
        dat: dat
      });
    } else {
      dat.main = dat.issue;
      console.log("================");
      console.log("from MainDialog MIDDLE");
      console.log("================");
      return await stepContext.replaceDialog(Main_Dialog2, {
        dat: dat
      });
      //return await stepContext.endDialog();
    }
  }

  async lastStep(stepContext) {
    dat.main = dat.issue;
    console.log("================");
    console.log("from MainDialog END!!!!!!");
    console.log("================");
    return await stepContext.replaceDialog(Main_Dialog2, {
      dat: dat
    });
  }

  //Include different cases within it.

  /**
   * This is the final step in the main waterfall dialog.
   * It wraps up the sample "book a flight" interaction with a simple confirmation.
   */
}
module.exports.MainDialog = MainDialog;
module.exports.main_dialog = main_dialog;
