const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { gamecard } = require("../cards/gameper");
const fs = require("fs");
const Performance_Dialog = "Performance_Dialog";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const Search = require("./bingsearch");
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { Feedback, feedback } = require("./Feedback");
const { Contact, contact } = require("./Contact.js");
const { MainDialog, main_dialog } = require("./mainDialog");
const { MainDialog2, Main_Dialog2 } = require("./mainDialog2");
var dat;
const Printer_Issue = "Printer_Issue";
const Printer_Wifi_Issue = "Printer_Wifi_Issue";
var a, b, c, d, aa, bb, cc, dd, msg, fil;
var solve_yes1, solve_yes2, solve_no1, solve_no2;

class Printer_Wifi_Dialog extends ComponentDialog {
  constructor(conversationState, userState) {
    super(Printer_Wifi_Issue);

    // The state management objects for the conversation and user state.
    this.conversationState = conversationState;
    this.userState = userState;

    // Define the main dialog and its related components.

    this.addDialog(new Feedback());
    this.addDialog(new Contact());
    this.addDialog(new DisplaySteps());
    // this.addDialog(new MainDialog2());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.performanceStep.bind(this),
        this.lastStep.bind(this),
        this.lastlaststep.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async initialStep(stepContext) {
    dat = stepContext.options.dat;
    console.log("printer wifi 1");
    msg = "What wireless printing issue are you experiencing?";
    a = aa = "Printer does not maintain its wireless connection";
    b = bb = "Connecting to my wireless network";
    c = cc = "Connect the printer to a new or different wireless network";
    if (dat.lang != "en") {
      await Translate.translate(msg, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg = value;
      });
      await Translate.translate(a, dat.lang).then(function(value) {
        console.log("converterd " + value);
        a = value;
      });
      await Translate.translate(b, dat.lang).then(function(value) {
        console.log("converterd " + value);
        b = value;
      });
      await Translate.translate(c, dat.lang).then(function(value) {
        console.log("converterd " + value);
        c = value;
      });
    }
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: a,
                      data: a
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: b,
                      data: b
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: c,
                      data: c
                    }
                  ]
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });

    return await stepContext.prompt("textPrompt", {});
  }
  async performanceStep(stepContext) {
    var temp = stepContext.result;
    console.log("wifi 2  =====" + temp + a);
    console.log(temp);
    console.log(b);
    console.log(aa);
    //dat.printer_wifi = stepContext.result;

    // if (temp == a) {
    //   console.log("onnonononn");
    //   dat.printer_wifi = aa;
    // } else if (stepContext.result == b) {
    //   dat.printer_wifi = bb;
    // } else if (stepContext.result == c) {
    //   dat.printer_wifi = cc;
    // }

    if (dat.lang != "en") {
      if (stepContext.result == a) {
        dat.printer_wifi = aa;
      } else if (stepContext.result == b) {
        dat.printer_wifi = bb;
      } else if (stepContext.result == c) {
        dat.printer_wifi = cc;
      } else {
        dat.printer_wifi = stepContext.result;
      }
    } else {
      dat.printer_wifi = stepContext.result;
    }

    console.log(dat.printer_wifi + "  lalalaalalal");

    if (
      dat.printer_wifi == "Printer does not maintain its wireless connection"
    ) {
      fil = "01";
    } else if (dat.printer_wifi == "Connecting to my wireless network") {
      fil = "02";
    } else if (
      dat.printer_wifi ==
      "Connect the printer to a new or different wireless network"
    ) {
      fil = "03";
    } else {
      dat.main = dat.printer_wifi;
      return await stepContext.replaceDialog(Main_Dialog2, {
        dat: dat
      });
    }
    var filename = "./printerjson/" + fil + ".json";
    let f2 = fs.readFileSync(filename);
    var jsc;
    eval("jsc = " + f2);
    var key = "Solve";
    var ab = jsc[key];
    var len = ab.length;
    for (var q = 0; q < len; q++) {
      var lan = ab[q];
      if (dat.lang != "en") {
        await Translate.translate(lan, dat.lang).then(function(value) {
          console.log("converterd " + value);
          lan = value;
        });
      }

      await stepContext.context.sendActivity(lan);
    }

    var msg_solve = "Did it solve the problem";

    solve_no1 = solve_no2 = "No";
    solve_yes1 = solve_yes2 = "Yes";
    if (dat.lang != "en") {
      await Translate.translate(msg_solve, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg_solve = value;
      });
      await Translate.translate(solve_no1, dat.lang).then(function(value) {
        console.log("converterd " + value);
        solve_no1 = value;
      });
      await Translate.translate(solve_yes1, dat.lang).then(function(value) {
        console.log("converterd " + value);
        solve_yes1 = value;
      });
    }
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg_solve
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: solve_yes1,
                      data: solve_yes1
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: solve_no1,
                  data: solve_no1
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    return await stepContext.prompt("textPrompt", {});
  }
  async lastStep(stepContext) {
    var ans = stepContext.result;
    if (ans == solve_yes1) {
      ans = "Yes";
    } else if (ans == solve_no1) {
      ans = "No";
    }
    console.log(ans);
    if (ans == "Yes") {
      //await stepContext.context.sendActivity("Now entering feedback dialog");
      return await stepContext.replaceDialog(feedback, {
        dat: dat
      });
    } else if (ans == "No") {
      var filename = "./printerjson/" + fil + ".json";
      let f2 = fs.readFileSync(filename);
      var jsc;
      eval("jsc = " + f2);
      var key = "Alt_sol";
      var ab = jsc[key];
      var len = ab.length;
      var secsoln = "here's a second set of solutions";
      await stepContext.context.sendActivity(secsoln);
      for (var q = 0; q < len; q++) {
        var lan = ab[q];
        if (dat.lang != "en") {
          await Translate.translate(lan, dat.lang).then(function(value) {
            console.log("converterd " + value);
            lan = value;
          });
        }

        await stepContext.context.sendActivity(lan);
      }
      //await stepContext.context.sendActivity("Now entering contact dialog");
      var msg_solve = "Did it solve the problem";
      solve_no1 = solve_no2 = "No";
      solve_yes1 = solve_yes2 = "Yes";
      if (dat.lang != "en") {
        await Translate.translate(msg_solve, dat.lang).then(function(value) {
          console.log("converterd " + value);
          msg_solve = value;
        });
        await Translate.translate(solve_no1, dat.lang).then(function(value) {
          console.log("converterd " + value);
          solve_no1 = value;
        });
        await Translate.translate(solve_yes1, dat.lang).then(function(value) {
          console.log("converterd " + value);
          solve_yes1 = value;
        });
      }
      await stepContext.context.sendActivity({
        text: "",
        attachments: [
          CardFactory.adaptiveCard({
            type: "AdaptiveCard",
            version: "1.0",
            body: [
              {
                type: "Container",
                items: [
                  {
                    type: "TextBlock",
                    text: msg_solve
                  },
                  {
                    type: "ActionSet",
                    actions: [
                      {
                        type: "Action.Submit",
                        title: solve_yes1,
                        data: solve_yes1
                      }
                    ]
                  }
                ]
              },
              {
                type: "ActionSet",
                actions: [
                  {
                    type: "Action.Submit",
                    title: solve_no1,
                    data: solve_no1
                  }
                ]
              }
            ],
            $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
          })
        ]
      });
      return await stepContext.prompt("textPrompt", {});
    } else {
      dat.main = "printer issue";
      return await stepContext.replaceDialog(Main_Dialog2, {
        dat: dat
      });
    }
  }
  async lastlaststep(stepContext) {
    var ans = stepContext.result;
    if (ans == solve_yes1) {
      ans = "Yes";
    } else if (ans == solve_no1) {
      ans = "No";
    }
    console.log(ans);
    if (ans == "Yes") {
      //await stepContext.context.sendActivity("Now entering feedback dialog");
      return await stepContext.replaceDialog(feedback, {
        dat: dat
      });
    } else if (ans == "No") {
      //await stepContext.context.sendActivity("Now entering contact dialog");
      return await stepContext.replaceDialog(contact, {
        dat: dat
      });
    } else {
      dat.main = ans;
      return await stepContext.replaceDialog(Main_Dialog2, {
        dat: dat
      });
    }
  }
}
module.exports.Printer_Wifi_Dialog = Printer_Wifi_Dialog;
module.exports.Printer_Wifi_Issue = Printer_Wifi_Issue;
