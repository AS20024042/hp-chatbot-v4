// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  BotFrameworkAdapter
} = require("botbuilder");
const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ChoicePrompt,

  ComponentDialog,
  DialogSet,
  DialogTurnStatus,
  WaterfallDialog
} = require("botbuilder-dialogs");
const { UserProfile } = require("../userProfile");
const { TextPrompt } = require("botbuilder-dialogs");
const { LuisHelper } = require("./luisHelper");
const { PerformanceDialog, Performance_Dialog } = require("./Performance.js");
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { ConfirmSteps, confirm_steps } = require("./confirmSteps.js");
const { PrinterDialog, Printer_Issue } = require("./Printer.js");
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const { Contact, contact } = require("./Contact.js");
const { Feedback, feedback } = require("./Feedback");
const Main_Dialog2 = "Main_Dialog2";
const TEXT_PROMPT = "textPrompt";
//const { MainDialog2, Main_Dialog2 } = require("./mainDialog2");
const {
  Printer_Wifi_Dialog,
  Printer_Wifi_Issue
} = require("./Printer_Wifi_Issue");
var dat;
const {
  Printer_Job_Stuck_Issue,
  Printer_Job_Stuck_Dialog
} = require("./Printer_Job_Stuck.js");
const {
  Printer_Offline_Issue,
  Printer_Offline_Dialog
} = require("./Printer_Offline.js");
const {
  Printer_Look_Dialog,
  Printer_Look_Issue
} = require("./Printer_Look.js");
const { ServiceNow, service_now } = require("./ServiceNow.js");

class MainDialog2 extends ComponentDialog {
  constructor() {
    super(Main_Dialog2);
    // Define the main dialog and its related components.
    // This is a sample "book a flight" dialog.
    this.addDialog(new PerformanceDialog());
    this.addDialog(new DisplaySteps());
    //this.addDialog(new PrinterDialog());
    //this.addDialog(new Printer_Wifi_Dialog());
    // this.addDialog(new Printer_Job_Stuck_Dialog());
    // this.addDialog(new Printer_Offline_Dialog());
    // this.addDialog(new Printer_Look_Dialog());
    this.addDialog(new ServiceNow());
    this.addDialog(new ConfirmSteps());
    this.addDialog(new Contact());

    this.addDialog(new Feedback());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.luisStep.bind(this),
        this.choiceStep.bind(this),
        this.laststep.bind(this)
        //this.finalStep.bind(this)
      ])
    );
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async luisStep(stepContext) {
    dat = stepContext.options.dat;
    if (dat.main == null) {
      dat.main = "";
    }

    console.log("@@@@@@@@   main2    @@@@@@@@" + dat.main);
    //Connect with mongoDB and send the card mentioning user details like name, device information etc.
    if (
      !process.env.LuisAppId ||
      !process.env.LuisAPIKey ||
      !process.env.LuisAPIHostName
    ) {
      await stepContext.context.sendActivity(
        "NOTE: LUIS is not configured. To enable all capabilities, add `LuisAppId`, `LuisAPIKey` and `LuisAPIHostName` to the .env file."
      );
    }
    return await stepContext.next();

    // return await stepContext.prompt('Text_Prompt', { prompt: 'Please describe the problem you are currently experiencing.' });
  }

  async choiceStep(stepContext) {
    let user_query = {};
    if (
      process.env.LuisAppId &&
      process.env.LuisAPIKey &&
      process.env.LuisAPIHostName
    ) {
      //Microsoft Translator.
      // var userEntry = stepContext.context._activity.text;
      dat = stepContext.options.dat;
      var userEntry = stepContext.context._activity.text;
      console.log(dat.main + " in main 2");
      await Detect.detect(dat.main)
        .then(function(value) {
          console.log("the lang is =  " + value);
          dat.lang = value;
          console.log("the lang is =  " + dat.lang);
        })
        .then(function(value) {
          if (dat.lang != "en") {
            console.log("its not eng");
          }
        })
        .then(function(value) {
          console.log("end of detect");
        });
      if (dat.lang != "en") {
        await Translate.translate(dat.main, "en")
          .then(function(conv) {
            console.log("the conv is ===  " + conv);
            dat.main = conv;
            stepContext.context._activity.text = dat.main;
            return conv;
          })
          .then(function(value) {
            console.log("end of trans");
          });
      }
      //dat.main = stepContext.context;

      console.log(stepContext.context._activity.text, "   000000000000000");
      user_query = await LuisHelper.executeLuisQuery(stepContext.context);

      console.log("LUIS extracted these details:", user_query);
      if (user_query.intent == "Performance_Games") {
        dat.completepath.push("Performance issue with games");
        return await stepContext.beginDialog(Performance_Dialog, {
          top: user_query.intent,
          dat: dat
        });
        //return await stepContext.endDialog();
      } else if (user_query.intent == "printer") {
        console.log("printer+++");
        return await stepContext.beginDialog("Printer_Issue", {
          top: user_query.intent,
          dat: dat
        });
      } else if (user_query.intent == "printer_wifi_issue") {
        return await stepContext.replaceDialog("Printer_Wifi_Issue", {
          top: user_query.intent,
          dat: dat
        });
      } else if (user_query.intent == "printer_job_stuck") {
        return await stepContext.replaceDialog(Printer_Job_Stuck_Issue, {
          dat: dat
        });
      } else if (user_query.intent == "printer_offline") {
        return await stepContext.replaceDialog(Printer_Offline_Issue, {
          dat: dat
        });
      } else if (user_query.intent == "printer_not_look_good") {
        return await stepContext.replaceDialog(Printer_Look_Issue, {
          dat: dat
        });
      } else if (user_query.intent == "service_now_ticket_creation") {
        return await stepContext.replaceDialog(service_now, {
          dat: dat
        });
      } else if (user_query.intent == "anger") {
        // await stepContext.context.sendActivity(
        //   "anger detected grrrrrr 😠😠😠😠😠😠😠"
        // );
        return await stepContext.beginDialog(contact, {
          dat: dat
        });
      } else {
        if (dat.wrong < 2) {
          dat.wrong = dat.wrong + 1;
          var didntUnderstandMessageText = `Sorry, I didn't get that`;

          var msg = "Rephrase the text please!!";
          if (dat.lang != "en") {
            await Translate.translate(msg, dat.lang).then(function(value) {
              console.log("converterd " + value);
              msg = value;
            });
            await Translate.translate(
              didntUnderstandMessageText,
              dat.lang
            ).then(function(value) {
              console.log("converterd " + value);
              didntUnderstandMessageText = value;
            });
          }
          await stepContext.context.sendActivity(didntUnderstandMessageText);

          await stepContext.context.sendActivity(msg);
          return await stepContext.prompt("textPrompt", {});
        } else {
          return await stepContext.replaceDialog(contact, {
            dat: dat
          });
        }
      }
    }
    return await stepContext.endDialog();
  }
  async laststep(stepContext) {
    var text = stepContext.result;
    dat.main = text;
    console.log(text, "loploplop");
    console.log("=====================================");
    console.log("from MainDialog2 only-- internal call");
    console.log("======================================");
    return await stepContext.replaceDialog(Main_Dialog2, {
      dat: dat
    });
  }

  //Include different cases within it.

  /**
   * This is the final step in the main waterfall dialog.
   * It wraps up the sample "book a flight" interaction with a simple confirmation.
   */
}
module.exports.MainDialog2 = MainDialog2;
module.exports.Main_Dialog2 = Main_Dialog2;
