// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
var config = require("./config");
var https = require("https");
var async = require("async");
var removeValue = require("remove-value");
const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
var unique = require("array-unique");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
//const { CancelAndHelpDialog } = require('./cancelAndHelpDialog');
//const { DateResolverDialog } = require('./dateResolverDialog');
const { UserProfile } = require("../userProfile");
const display_steps = "display_steps";
const second_steps = "second_steps";
const CONFIRM_PROMPT = "confirmPrompt";
const grem = require("./grem.js");
//const DATE_RESOLVER_DIALOG = 'dateResolverDialog';
//const TEXT_PROMPT = 'textPrompt';
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Translate = require("./Translate.js");
const { ShowSteps, show_steps } = require("./showSteps.js");
const { ResolveIssue, resolve_issue } = require("./resolveIssue.js");
const { ConfirmSteps, confirm_steps } = require("./confirmSteps.js");

//var PythonShell = require("python-shell");
const { PythonShell } = require("python-shell");
var unique = require("array-unique");
var Gremlin = require("gremlin");
var dat, val0, val1, val2, st;
var tempchoice;
var fin = 1;

class SecondSteps extends ComponentDialog {
  constructor() {
    super(second_steps);

    // Define the main dialog and its related components.
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new ConfirmSteps());
    this.addDialog(new ShowSteps());
    this.addDialog(new ResolveIssue());
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.secondStepPath.bind(this),
        this.secondStepPath2.bind(this),
        this.secondStepPath3.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async secondStepPath(stepContext) {
    console.log("entered secondstep 1st part");
    const msg_secondSteps = "Choose one amongst the following";
    dat = stepContext.options.dat;
    var id = dat.latestnode;
    var options;

    dat.finalstring = [];

    // start here

    await grem
      .grem(dat)
      .then(function(value) {
        console.log("this is from the grem file=======" + value);
        dat.results = value;
        return value;
      })
      .then(function(value) {
        //console.log(typeof value);
        unique(value);
        var results = value;
        console.log("this is after unique " + value);
        if (results.length == 0) {
          fin = 0;
          console.log("=======  gremlin finish . going to contact==========");
        } else {
          fin = 1;
          if (results.includes("ways")) {
            Array.prototype.remove = removeValue;
            results.remove("ways");
          }
          console.log("results= ", results);
          for (var j = 0; j <= results.length - 1; j++) {
            dat.finalstring.push(results[j]);
          }
          console.log("this is final string  " + dat.finalstring);
          st = dat.finalstring;
          // return st;
        }
      });
    console.log("second 1== length==  " + st.length);

    // options = {
    //   prompt: "blah blah",
    //   retryPrompt:
    //     "That was not a valid choice, please choose the emergency type.",
    //   choices: this.getProblemChoices(st)
    // };
    //return await stepContext.prompt("cardPrompt", options);
    if (fin == 0) {
      return await stepContext.replaceDialog("contact", {
        dat: dat
      });
    } else {
      if (st.length == 2) {
        val0 = st[0];
        val1 = st[1];
        if (dat.lang != "en") {
          await Translate.translate(val1, dat.lang).then(function(value) {
            console.log("converterd " + value);
            val1 = value;
          });
          await Translate.translate(val0, dat.lang).then(function(value) {
            console.log("converterd " + value);
            val0 = value;
          });
        }
        await stepContext.context.sendActivity({
          text: "",
          attachments: [
            CardFactory.adaptiveCard({
              type: "AdaptiveCard",
              version: "1.0",
              body: [
                {
                  type: "Container",
                  items: [
                    {
                      type: "TextBlock",
                      text: ""
                    },
                    {
                      type: "ActionSet",
                      actions: [
                        {
                          type: "Action.Submit",
                          title: val0,
                          data: val0
                        }
                      ]
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: val1,
                      data: val1
                    }
                  ]
                }
              ],
              $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
            })
          ]
        });
        return await stepContext.prompt("textPrompt", {});
      } else {
        val0 = st[0];
        val1 = st[1];
        val2 = st[2];
        if (dat.lang != "en") {
          await Translate.translate(val1, dat.lang).then(function(value) {
            console.log("converterd " + value);
            val1 = value;
          });
          await Translate.translate(val0, dat.lang).then(function(value) {
            console.log("converterd " + value);
            val0 = value;
          });
          await Translate.translate(val2, dat.lang).then(function(value) {
            console.log("converterd " + value);
            val2 = value;
          });
        }
        await stepContext.context.sendActivity({
          text: "",
          attachments: [
            CardFactory.adaptiveCard({
              type: "AdaptiveCard",
              version: "1.0",
              body: [
                {
                  type: "Container",
                  items: [
                    {
                      type: "TextBlock",
                      text: ""
                    },
                    {
                      type: "ActionSet",
                      actions: [
                        {
                          type: "Action.Submit",
                          title: val0,
                          data: val0
                        }
                      ]
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: val1,
                      data: val1
                    }
                  ]
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: val2,
                      data: val2
                    }
                  ]
                }
              ],
              $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
            })
          ]
        });
        return await stepContext.prompt("textPrompt", {});
      }
    }
  }
  async secondStepPath2(stepContext) {
    tempchoice = stepContext.result;
    console.log("second 2== " + tempchoice);
    return await stepContext.next();
  }
  async secondStepPath3(stepContext) {
    console.log("temp choice  -------  val0 -------  val1");
    console.log(tempchoice + val0 + val1 + st[0] + st[1]);
    if (tempchoice == val0) {
      tempchoice = st[0];
    } else if (tempchoice == val1) {
      tempchoice = st[1];
    } else {
      tempchoice = st[2];
    }
    console.log("second 3== " + tempchoice);
    //console.log("user selected @@@@@  " + stepContext.result.value);
    return await stepContext.beginDialog("resolve_issue", {
      dat: dat,
      arg: tempchoice
    });
  }

  // gremlinconnect() {
  //   return new Promise(function(resolve, reject) {
  //     global.client1 = Gremlin.createClient(443, config.endpoint, {
  //       session: false,
  //       ssl: true,
  //       user: `/dbs/${config.database}/colls/${config.collection}`,
  //       password: config.primaryKey
  //     });
  //     // if (global.client1) {
  //     //   resolve(global.client1);
  //     // } else {
  //     //   reject("Can't connect with cosmos db client");
  //     // }
  //   });
  // }

  // getedgelabels(id) {
  //   let getlabels = "g.V(id1).outE().values('label')";
  //   getlabels = getlabels.replace("id1", "'" + id + "'");
  //   console.log("label first =   " + getlabels);
  //   global.client1.execute(getlabels, {}, (err, results) => {
  //     if (err) {
  //       return err;
  //       //callback(console.error(err));
  //     }

  //     //console.log("Result: %s\n", JSON.stringify(results));
  //     console.log("This is result");
  //     console.log(results);
  //     return results;
  //     // callback(results);
  //   });
  // }

  getProblemChoices(st) {
    var cardOptions;
    if (st.length == 2) {
      cardOptions = [
        {
          value: st[0],
          synonyms: ["yes"]
        },
        {
          value: st[1],
          synonyms: ["no"]
        }
      ];
    } else {
      cardOptions = [
        {
          value: st[0],
          synonyms: ["yes"]
        },
        {
          value: st[1],
          synonyms: ["no"]
        },
        {
          value: st[2],
          synonyms: [""]
        }
      ];
    }

    return cardOptions;
  }
}

module.exports.SecondSteps = SecondSteps;
module.exports.second_steps = second_steps;
