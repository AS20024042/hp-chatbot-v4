/* This simple app uses the '/translate' resource to translate text from
one language to another. */

/* This template relies on the request module, a simplified and user friendly
way to make HTTP requests. */
const request = require("request");
const uuidv4 = require("uuid/v4");
const fs = require("fs");

var subscriptionKey = "97ac7f508c2647bb85ae6e10827494ca";
var endpoint =
  "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0";
var obj;

/* If you encounter any issues with the base_url or path, make sure that you are
using the latest endpoint: https://docs.microsoft.com/azure/cognitive-services/translator/reference/v3-0-translate */
const translate = function translateText(tet, lang) {
  return new Promise((resolve, reject) => {
    var subscriptionKey = "97ac7f508c2647bb85ae6e10827494ca";
    var endpoint =
      "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0";
    var obj;
    let options = {
      method: "POST",
      baseUrl: endpoint,
      url: "translate",
      qs: {
        "api-version": "3.0",
        to: [lang]
      },
      headers: {
        "Ocp-Apim-Subscription-Key": subscriptionKey,
        "Content-type": "application/json",
        "X-ClientTraceId": uuidv4().toString()
      },
      body: [
        {
          text: tet
        }
      ],
      json: true
    };
    request(options, function(err, res, body) {
      //console.log(JSON.stringify(body, null, 4));
      obj = body;
      var a = body[0];
      var b = a["translations"];
      var c = b[0];
      var d = c["text"];
      //console.log(d);

      resolve(d);
    });
  });
};

// Call the function to translate text.
//var tet = "my name is nitin";
//translateText(tet, lang);
module.exports.translate = translate;
