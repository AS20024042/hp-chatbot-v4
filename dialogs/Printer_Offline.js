const {
  TimexProperty
} = require("@microsoft/recognizers-text-data-types-timex-expression");
const {
  ConfirmPrompt,
  TextPrompt,
  WaterfallDialog
} = require("botbuilder-dialogs");
const {
  ActivityHandler,
  ActionTypes,
  ActivityTypes,
  AttachmentLayoutTypes,
  CardFactory,
  MessageFactory
} = require("botbuilder");
const {
  ChoicePrompt,
  ComponentDialog,
  DialogSet,
  DialogTurnStatus
} = require("botbuilder-dialogs");
const { gamecard } = require("../cards/gameper");
const fs = require("fs");
const Performance_Dialog = "Performance_Dialog";
const TEXT_PROMPT = "textPrompt";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";
const Detect = require("./Detect.js");
const Translate = require("./Translate.js");
const Search = require("./bingsearch");
const { DisplaySteps, display_steps } = require("./displaysteps.js");
const { Feedback, feedback } = require("./Feedback");
const { Contact, contact } = require("./Contact.js");
const { LoopImage, loop_image } = require("./loopImage.js");
var dat;
const Printer_Issue = "Printer_Issue";
const Printer_Wifi_Issue = "Printer_Wifi_Issue";
const Printer_Offline_Issue = "Printer_Offline_Issue";
const { MainDialog2, Main_Dialog2 } = require("./mainDialog2");
var a, b, c, d, aa, bb, cc, dd, msg;
var solve_yes1, solve_yes2, solve_no1, solve_no2;

class Printer_Offline_Dialog extends ComponentDialog {
  constructor(conversationState, userState) {
    super(Printer_Offline_Issue);

    // The state management objects for the conversation and user state.
    this.conversationState = conversationState;
    this.userState = userState;

    // Define the main dialog and its related components.

    this.addDialog(new Feedback());
    this.addDialog(new Contact());
    this.addDialog(new DisplaySteps());
    this.addDialog(new LoopImage());
    this.addDialog(new ChoicePrompt("cardPrompt"));
    this.addDialog(new TextPrompt(TEXT_PROMPT));
    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.performanceStep.bind(this),
        this.lastStep.bind(this)
      ])
    );

    // The initial child Dialog to run.
    this.initialDialogId = WATERFALL_DIALOG;
  }

  async initialStep(stepContext) {
    dat = stepContext.options.dat;
    console.log("printer offline 1");
    msg = "What operating system are you using";

    if (dat.lang != "en") {
      await Translate.translate(msg, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg = value;
      });
    }

    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: "Windows",
                      data: "Windows"
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: "Mac",
                  data: "Mac"
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });

    return await stepContext.prompt("textPrompt", {});
  }
  async performanceStep(stepContext) {
    dat.printer_offline = stepContext.result;
    console.log(dat.printer_offline);
    // return await stepContext.replaceDialog(Printer_Offliine_Issue, {
    //   dat: dat
    // });
    var fil;
    if (dat.printer_offline == "Windows") {
      fil = "30";
    } else if (dat.printer_offline == "Mac") {
      fil = "31";
    } else {
      dat.main = dat.printer_offline;
      return await stepContext.replaceDialog("Main_Dialog2", {
        dat: dat
      });
    }

    var filename = "./printerjson/" + fil + ".json";
    let f2 = fs.readFileSync(filename);
    var jsc;
    eval("jsc = " + f2);
    var key = "Solve";
    var ab = jsc[key];
    var len = ab.length;
    for (var q = 0; q < len; q++) {
      var lan = ab[q];
      lan = lan.toString();
      var r2 = lan.match(/https:[^\s]+.jpg/g);

      if (dat.lang != "en") {
        await Translate.translate(lan, dat.lang).then(function(value) {
          console.log("converterd " + value);
          lan = value;
        });
      }

      if (r2 != null) {
        lan = lan.replace(/https:[^\s]+.jpg/g, "");
        // r2.forEach(function(element) {});
        console.log("Starting loopImage===== of showsteps");
        //var element = JSON.stringify(r2);
        var element = r2[0];
        console.log(typeof element, element);
        //await stepContext.beginDialog(loop_image, { rnew: element });
        await stepContext.context.sendActivity({
          text: "",
          attachments: [
            CardFactory.adaptiveCard({
              type: "AdaptiveCard",
              version: "1.0",
              body: [
                {
                  type: "Image",
                  altText: "",
                  url: element
                  // "https://hp.dezide.com/data/pictures/hp-external/web_author/guid-4ded5cad-ffb6-4783-bdf9-3719ade50ed7-low_55920_en.jpg"
                }
              ],
              $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
            })
          ]
        });
      } else {
        //await stepContext.context.sendActivity(lan);
      }
      await stepContext.context.sendActivity(lan);
    }
    var msg_solve = "Did it solve the problem";
    console.log("offline yes no part");
    solve_no1 = solve_no2 = "No";
    solve_yes1 = solve_yes2 = "Yes";
    if (dat.lang != "en") {
      await Translate.translate(msg_solve, dat.lang).then(function(value) {
        console.log("converterd " + value);
        msg_solve = value;
      });
      await Translate.translate(solve_no1, dat.lang).then(function(value) {
        console.log("converterd " + value);
        solve_no1 = value;
      });
      await Translate.translate(solve_yes1, dat.lang).then(function(value) {
        console.log("converterd " + value);
        solve_yes1 = value;
      });
    }
    await stepContext.context.sendActivity({
      text: "",
      attachments: [
        CardFactory.adaptiveCard({
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "Container",
              items: [
                {
                  type: "TextBlock",
                  text: msg_solve
                },
                {
                  type: "ActionSet",
                  actions: [
                    {
                      type: "Action.Submit",
                      title: solve_yes1,
                      data: solve_yes1
                    }
                  ]
                }
              ]
            },
            {
              type: "ActionSet",
              actions: [
                {
                  type: "Action.Submit",
                  title: solve_no1,
                  data: solve_no1
                }
              ]
            }
          ],
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json"
        })
      ]
    });
    console.log("offline yes no part============== end");
    return await stepContext.prompt("textPrompt", {});
  }
  async lastStep(stepContext) {
    var ans = stepContext.result;
    console.log(ans, "lolp");
    if (ans == solve_yes1) {
      ans = "Yes";
    } else if (ans == solve_no1) {
      ans = "No";
    }
    if (ans == "Yes") {
      console.log("offfline feedback lalalalala");
      //await stepContext.context.sendActivity("Now entering feedback dialog");
      return await stepContext.beginDialog(feedback, {
        dat: dat
      });
    } else if (ans == "No") {
      //await stepContext.context.sendActivity("Now entering contact dialog");
      return await stepContext.beginDialog(contact, {
        dat: dat
      });
    } else {
      dat.main = ans;
      return await stepContext.replaceDialog("Main_Dialog2", {
        dat: dat
      });
    }
  }
}
module.exports.Printer_Offline_Dialog = Printer_Offline_Dialog;
module.exports.Printer_Offline_Issue = Printer_Offline_Issue;
