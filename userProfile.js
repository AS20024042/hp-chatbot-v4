// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

class UserProfile {
  constructor(choice, latestnode) {
    this.choice = choice;
    this.latestnode = latestnode;

    // The list of companies the user wants to review.
    this.companiesToReview = [];
    this.text;
    this.splittext = [];
    this.plainText = "";
    this.count;
    this.continue = "";
    this.cho = [];
    this.position;
    this.finalstring = [];
    this.completepath = [];
    this.results;
    this.datajson;
  }
}

module.exports.UserProfile = UserProfile;
