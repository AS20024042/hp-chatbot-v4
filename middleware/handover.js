const { ActivityTypes, TurnContext, BotFrameworkAdapter,CardFactory } = require('botbuilder');
const schema=require('../Mongooseschema');
const AdaptiveCard=require('../public/history_card.json')
//const { LuisHelper } = require("luisHelper");
const UserState = {
  Bot: 'BOT',
  Queued: 'QUEUED',
  Agent: 'AGENT'
}

class HandoverMiddleware {

  /**
   * 
   * @param {ArrayHandoverProvider} provider 
   * @param {BotFrameworkAdapter} adapter 
   */
  constructor(provider, adapter) {
    this.provider = provider;
    this.adapter = adapter;
  }

  /**
   * 
   * @param {TurnContext} turnContext 
   * @param {*} next 
   */
  async onTurn(turnContext, next) {
   /* 
     ___  ___
    /   \/    \ 
   / |  05   | \
  / /|KING K |\ \ 
     |_______| 
    
    */
     await turnContext.sendActivity({type: 'typing'});
     console.log("Middleware Executing");

    if (turnContext.activity.type !== ActivityTypes.Message) { return await next(); }
    
    
    if (turnContext.activity.from.name.toLowerCase().startsWith("agent")) {
      await this.manageAgent(turnContext, next);
    } else {
      await this.manageUser(turnContext, next);
    }    
  }

  /**
   * 
   * @param {ArrayHandoverProvider} turnContext
   * @param {*} next 
   */
  async manageUser(turnContext, next) 
  {
    const conversationReference = TurnContext.getConversationReference(turnContext.activity);
    console.log("This is conversation reference",conversationReference);
    const { activity: { text }} = turnContext;
    //const user_query = await LuisHelper.executeLuisQuery(turnContext.activity);
  //  text=user_query.intent
    
    
    await this.provider.addToTranscript(conversationReference,text,turnContext)
    const user = await this.provider.findOrCreate(conversationReference);
    console.log(turnContext.activity);
                  
    // await this.provider.log((user, conversationReference.user.name, turnContext.activity.text));

  
    console.log(text);
    if (user.state === UserState.Agent) {
      return await this.adapter.continueConversation(user.agentReference, async agentContext => {
        await agentContext.sendActivity(turnContext.activity.text);
      });
    }

    switch(text.toLowerCase()) {
      case "agent":
        await this.provider.queueForAgent(conversationReference);
        await turnContext.sendActivity("Please wait while we try to connect you to an agent.")
        break;
      case "cancel":
        await this.provider.unqueueForAgent(conversationReference);
        await turnContext.sendActivity("You are now reconnected to the bot!");
        break;
        /*
      case "history":
       const history=await this.provider.fetchHistory(conversationReference);
        console.log("history",history)
        
        //history.forEach(history=>{turnContext.sendActivity(history.message)})
        history.forEach(async(history)=>{
          AdaptiveCard.body[0].text=history.message
          console.log(AdaptiveCard.body[0].text)
          AdaptiveCard.body[1].text="From: "+`${conversationReference.user.name}`
          console.log(AdaptiveCard.body[1].text)          
         await turnContext.sendActivity({attachments:[this.provider.sendAdaptiveCard()]})})
        break;
        */
      default:
        return await next();
      
      
    }
  }

  /**
   * 
   * @param {TurnConext} turnContext 
   * @param {*} next 
   */
  async manageAgent(turnContext, next) {
    
    const conversationReference = TurnContext.getConversationReference(turnContext.activity);
    const user = await this.provider.findByAgent(conversationReference);
    const { activity: { text }} = turnContext;
   // const username_conversationid
    if(text.toLowerCase().startsWith("connect with"))
    {
        const val=text.split(' ');
        const username=val[2]
        const user1=await this.provider.connectToParticularUser(conversationReference,username)
        if(user1){
          await turnContext.sendActivity(`You have been connected to ${username}`)
          await turnContext.sendActivity("Type #history to check the conversation history of this user");
          
        
        }
        else{
          await turnContext.sendActivity("The user does not exist")
        }
      
      
        
               
    }

    if (user) {
      if (text === "#disconnect") {
        this.provider.disconnectFromAgent(conversationReference);
        await turnContext.sendActivity("You have been disconnected from the user.");
        await this.adapter.continueConversation(user.userReference, async userContext => {
          await userContext.sendActivity("The agent disconnected from the conversation. You are now reconnected to the bot.")
        });
        return
      } 
      else if(text==="#history"){
        const convoIdHistory=user.ConversationId;
        const history=await this.provider.fetchHistory(convoIdHistory);
        // history.forEach(async(history)=>{
        //      const val=history.message
        //   AdaptiveCard.body[0].text=history.message
        //   console.log(AdaptiveCard.body[0].text)
        //   AdaptiveCard.body[1].text="From: "+`${user.userReference.user.name}`
        //   console.log(AdaptiveCard.body[1].text)          
        //   return await turnContext.sendActivity({attachments:[this.provider.sendAdaptiveCard()]})})
          history.forEach(async (history)=>{
          const val=history.message
          console.log(val)
          await turnContext.sendActivity(val)
          })
          //await turnContext.sendActivity(val)})
         return 
    //       AdaptiveCard.body[0].text=history.message
    //       console.log(AdaptiveCard.body[0].text)
    //       AdaptiveCard.body[1].text="From: "+`${user.userReference.user.name}`
    //       console.log(AdaptiveCard.body[1].text)          
    //       return await turnContext.sendActivity({attachments:[this.provider.sendAdaptiveCard()]})})
    // //    return await turnContext.sendActivity({attachments:[this.provider.sendAdaptiveCard()]}))}}
            }
       else if (text.indexOf("#") === 0) {
        await turnContext.sendActivity("Command not valid when connect to user");
        return
      } 
         
       
      else {
        // await this.provider.log(user, conversationReference.user.name);
          return this.adapter.continueConversation(user.userReference, async turnContext => {
          await turnContext.sendActivity(text);
        });
      }
    }

    switch (text) {
      case "#list":
        const queue = this.provider.getQueue();
        if (queue.length !== 0) 
        {
          const message = queue.map(user => user.userReference.user.name).join('\n');
          await turnContext.sendActivity("Users:\n\n" + message);
          await turnContext.sendActivity("Type Connect with <user_name> to connect with particular customer");
        } else {
          await turnContext.sendActivity("There are no users in the Queue right now.");
        }
        break;
      case "#connect": 
        const user = await this.provider.connectToAgent(conversationReference);
        if (user) {
          await turnContext.sendActivity(`You are connected to ${user.userReference.user.name}`);
          await this.adapter.continueConversation(user.userReference, async userContext => {
            await userContext.sendActivity("You are now connected to an agent!");
          })
        } else {
          await turnContext.sendActivity("There are no users in the Queue right now.");
        }
      
    }
  }
}

class ArrayHandoverProvider {

  constructor(backingStore=[]) {
    this.backingStore = backingStore;
  }

  async findOrCreate(userReference) {
    let result = this.backingStore.find(u => u.userReference.user.id === userReference.user.id);

    if (result) {
      return result
    }
    const {conversation: {id}}=userReference
    result = {
      userReference,
      state: UserState.Bot,
      messages: [],
      ConversationId:id
    }
    console.log("This is backinstore",result)
    this.backingStore.push(result);
    return result;
  }

  async log(user, from, text) {
    user.messages.push({ from, text});
    console.log(this.backingStore)
    return user;
  }

  

  async findByAgent(agentReference) {
    const result = this.backingStore.find(u => u.agentReference && u.agentReference.user.id === agentReference.user.id);
    return result? result: null;
  }

  //connect to particular user
  async connectToParticularUser(agentReference,username){
    const user= this.backingStore.find(u=>u.userReference.user.name===username)
    user.state=UserState.Agent;
    user.queueTime=null;
    user.agentReference=agentReference;
    return user
   }
  async queueForAgent(userReference) {
    const user = await this.findOrCreate(userReference);
    user.state = UserState.Queued;
    user.queueTime = new Date();
    console.log(this.backingStore);
    return user;
  }

  async unqueueForAgent(userReference) {
    const user = await this.findOrCreate(userReference);
    user.state = UserState.Bot;
    user.queueTime = null;
    return user;
  }

  async connectToAgent(agentReference) {
    const queue = this.getQueue();
    if (queue.length > 0) {
      const user = queue[0];
      user.state = UserState.Agent;
      user.queueTime = null;
      user.agentReference = agentReference;
      return user;
    } 
  }

  async fetchUser(username){
    const user=this.backingStore.find(u=>u.userReference.user.name===username)
    if(user)
    {
      return user
    }
  }


  async disconnectFromAgent(agentReference) {
    const user = await this.findByAgent(agentReference);
    user.state = UserState.Bot;
    user.agentContext = null;
    user.queueTime = null;
    return user;
  }

  getQueue() {
    return this.backingStore
      .filter(u => u.state === UserState.Queued)
      .sort(u => u.queueTime.getTime());
  }
 sendAdaptiveCard(){
    return CardFactory.adaptiveCard(AdaptiveCard)
    //return CardFactory.adaptiveCard(AdaptiveCard)
  }
   //fetch history to be uncommented
  //async fetchHistory(conversationReference)
  //{
  //const {conversation}=conversationReference
  //const {id}=conversation
  //const fetchconvo=schema.Conversation
  
  //const document=await this.fetchDocument(id);
  //console.log("this is document",document)
  //return document.transcripts
  /*  fetchconvo.findOne({conversationId:"7ae67fc0-2881-11ea-a448-bd3c737c93c4|livechat"}).then(async(record)=>{
      //console.log(record.transcripts)  
      const records=await record.transcripts
      console.log("Function records",records)
      return await records
              }).catch(err=>{console.log(err)})*/
  //}
  async fetchHistory(histConvoId)
  {
    const id=histConvoId;
    const fetchconvo=schema.Conversation;
    const document=await this.fetchDocument(id);
    console.log("THis is document",document)
    return document.transcripts;
    
    
  }
 
async fetchDocument(id)
{
  const fetchconvo=schema.Conversation
  const value=await fetchconvo.findOne({conversationId:id})
  return value
}
  //DBCONNECTIONS
  async addToTranscript(conversationReference,message,turnContext){
    
    const createTranscript=schema.transcript;
    
    const newTranscript=new createTranscript({from:{id:conversationReference.user.id,
                                              name:conversationReference.user.name },
                                        to:{id:conversationReference.bot.id,
                                            name:conversationReference.bot.name},
                                        message:message });
    
    const {conversation}=turnContext.activity;
    const {id}=conversation;
    const array=[12,3,4,5,5,6,6]
      //const uri="mongodb+srv://aryamanpanth:aryaman123@cluster0-3xshn.mongodb.net/test?retryWrites=true&w=majority"
    //const client = new MongoClient(uri, { useNewUrlParser: true });
    // client.connect(err => {
     //this.connectToDB(client).then(async(collection) => {
      //console.log("connected to add transcript");
      //collection.findAll({conversationId:"4a2af890-27b7-11ea-a448-bd3c737c93c4|livechat"}).then((err,record)=>{
     console.log("this is id ",id)
      const addconvo=schema.Conversation
      const record=await this.fetchDocument(id)
        record.transcripts.push(newTranscript)
        record.save();
        console.log("Pushed successfully");
       // console.log(record)
      //record.transcripts.push(array);
      //   console.log("here");
     
      //client.close();
      
    
   


  }
    
  async connectToDB(client){
  return client.db("test").collection("devices")
  }

  
}

module.exports.ArrayHandoverProvider = ArrayHandoverProvider;
module.exports.HandoverMiddleware = HandoverMiddleware;