var LanguageDetect = require('languagedetect');
var lngDetector = new LanguageDetect();


//***************start of translator part
function detect(textgiven) {
    return new Promise((resolve, reject) => {
        var cd = lngDetector.detect(textgiven); //cd returns an json of possible langauge in the textgiven
        if(textgiven=='hi'||textgiven=='"hi"'){
            var s='en';
        }
        else{
        console.log(cd[0][0]);
        console.log("lang fact", cd); //top language in json
        var s = cd[0][0].slice(0, 2);
        }
        resolve(s);
    })
}

// function detect(textgiven) {
//     return new Promise((resolve, reject) => {
//         var cd = lang_detect.detect(textgiven); //cd returns an json of possible langauge in the textgiven
//         console.log(cd[0].lang);
//         console.log("lang fact", cd); //top language in json
//         var s = cd[0].lang;
//         resolve(s);
//     })
// }

function translate_lang(textgiven, userLang) //to convert LUIS output to user's language
{
    //console.log("inside translation")
    return new Promise((resolve, reject) => {
        var ltd = userLang;

        //ltd.trim();
        console.log("value of ltd", userLang);
        if (ltd == 'en' || ltd == '"en"') {
            resolve(textgiven); //if the lang is en ,it returns the textgiven
        } else {
            console.log("ltd value in else::::", ltd); // it call the NMT(wipro translator) 
            langdetect = "http://52.14.211.101/translator/translator_dev_json" +
                "?data=" + encodeURIComponent(textgiven) + "&tLang=French" + "&sLang=English"
            request.get({
                    url: langdetect
                }, //tlang means target lang & slang means source lang 
                function (err, response, body) {
                    var x = response.body;
                    var langobj = JSON.parse(x);
                    s = langobj['translated_text'];
                    console.log("response is", s);
                    console.log("Request time in ms for translation", response.elapsedTime);
                    console.log('statusCode:', response.statusCode);
                    resolve(s);
                })

        }

    }) //return data1;
}

function translate_lang_prompts(textgiven, userLang) //to transalate strings into english for LUIS to understand
{
    return new Promise((resolve, reject) => {
        //ltd = s;
        //ltd.trim();
        if (userLang == 'fr' || userLang == '"fr"') {
            langdetect = "http://52.14.211.101/translator/translator_dev_json" +
                "?data=" + encodeURIComponent(textgiven) + "&tLang=English" + "&sLang=French"
            request.get({
                    url: langdetect
                }, //tlang means target lang & slang means source lang 
                function (err, response, body) {
                    var x = response.body;
                    var langobj = JSON.parse(x)
                    s = langobj['translated_text'];
                    console.log("response is", s);
                    console.log("Request time in ms for translation", response.elapsedTime);
                    console.log('statusCode:', response.statusCode);
                    resolve(s);
                })

        } else {
            resolve(textgiven);
        }
        //return data1;
    })
}
//**********end of translator***********