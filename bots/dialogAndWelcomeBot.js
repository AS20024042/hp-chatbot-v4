// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { CardFactory } = require('botbuilder-core');
const { DialogBot } = require('./dialogBot');
const WelcomeCard = require('./resources/issue_card.json');
const CONVERSATION_DATA_PROPERTY = 'conversationData';
const schema=require('../Mongooseschema');
const USER_PROFILE_PROPERTY = 'userProfile';
class DialogAndWelcomeBot extends DialogBot {
    constructor(conversationState, userState, dialog) {
        super(conversationState, userState, dialog);
        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            const {channelId,conversation}=context.activity;
            const{id}=conversation
            let convoid=id
         //   await context.sendActivity("Hello, Welcome to the technical support center. I am here to help you with issues related to gaming and booting on your laptop. Please note, this chat will be recorded for quality and training purposes!!");
            for (let cnt = 0; cnt < membersAdded.length; cnt++) 
            {
                if (membersAdded[cnt].id !== context.activity.recipient.id) 
                {
                    const member_name=membersAdded[cnt].id;
                   const newconversation=schema.Conversation;
                   const addConversation=new newconversation({id:id,
                    name:member_name,
                    channelId:channelId,
                    conversationId:convoid,
                    transcripts:[]
                  })
                  console.log("This is context");
                 console.log();
                 
                //  else
                //  {
             //        await context.sendActivity("Hello, Welcome to the technical support center. I am here to help you with issues related to gaming and booting on your laptop. Please note, this chat will be recorded for quality and training purposes!!");
                // }
   
                   addConversation.save();
                  console.log("Data inserted successd")
               }
            }
            await next();
        });
    }
}

module.exports.DialogAndWelcomeBot = DialogAndWelcomeBot;
