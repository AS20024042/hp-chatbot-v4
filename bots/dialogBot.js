// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ActivityHandler } = require('botbuilder');

class DialogBot extends ActivityHandler
 {
    /**
     *
     * @param {ConversationState} conversationState
     * @param {UserState} userState
     * @param {Dialog} dialog
     */
    constructor(conversationState, userState, dialog) 
    {
        super();
        if (!conversationState) throw new Error('[DialogBot]: Missing parameter. conversationState is required');
        if (!userState) throw new Error('[DialogBot]: Missing parameter. userState is required');
        if (!dialog) throw new Error('[DialogBot]: Missing parameter. dialog is required');
        this.conversationState = conversationState;
        this.userState = userState;
        this.dialog = dialog;
        this.dialogState = this.conversationState.createProperty('DialogState');
        
        
        
        this.onMessage(async (context, next) => 
        {
            console.log('Running dialog with Message Activity.');
            await this.dialog.run(context, this.dialogState);
            await next();
        });
         // Handler for "event" activity
        this.onEvent(async (context, next) =>{
                if(context.activity.from.name.includes("agent"))
                 {
                    await context.sendActivity("Hello, Welcome to the Agent Portal. Please Type #list to check the waiting customers!!"); 
                 } 
                else
                {
                await context.sendActivity("Hello! Thank you for contacting HP technical support center. I am here to help you with issues related to printer and laptop performance.");
                await context.sendActivity("Please note that this chat will be recorded for quality and training purposes.");
                await context.sendActivity("Please Enter your name to proceed further.");
                }
        })

        this.onDialog(async (context, next) => {
            await this.conversationState.saveChanges(context, false);
            await this.userState.saveChanges(context, false);
            await next();
        });
    }
}

module.exports.DialogBot = DialogBot;
