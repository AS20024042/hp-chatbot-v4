 var feedbackcard = {
    'contentType': 'application/vnd.microsoft.card.adaptive',
    'content': {
	"$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
	"type": "AdaptiveCard",
	"version": "1.0",
	"body": [
		{
			"type": "ColumnSet",
			"columns": [
				{
					"type": "Column",
					"width": 2,
					"items": [
						{
							"type": "TextBlock",
							"text": "Please provide your valuable feedback.\n",
							"weight": "bolder",
							"size": "medium"
						},
						{
							"type": "TextBlock",
							"text": "You can rate our conversation on a scale of 1 to 5",
							"isSubtle": true,
							"wrap": true
						},
						
                        {
                            "type": "TextBlock",
                              "text": "Please select your rating level "
                        },
						{
                      "type": "Input.ChoiceSet",
                      "id": "rating",
                      "style": "expanded",
                      "value": "5",
                      "choices": [
                        {
                          "title": "⭐",
                          "value": "1"
                        },
                        {
                          "title": "⭐⭐",
                          "value": "2"
                        },
                        {
                          "title": "⭐⭐⭐",
                          "value": "3"
                        },
                        {
                          "title": "⭐⭐⭐⭐",
                          "value": "4"
                        },
                        {
                          "title": "⭐⭐⭐⭐⭐",
                          "value": "5"
                        }
                          ]
                        },
                        {
							"type": "TextBlock",
							"text": "Your additional comments go here.",
						},
						{
							"type":"Input.Text",
                             "id": "comment",
                             "isMultiline": true,
                             "placeholder": "Type here..."
						},
					]
				}
			]
		}
	],
	"actions": [
		{
			"type": "Action.Submit",
			"title": "Submit",
            'data': {
                     'type': 'feed'
                     }
		}
	]
}
    };
    
var fun=function(name,adid,laptopmodel,laptopno,warranty,email){
   var infocard={
      'contentType': 'application/vnd.microsoft.card.adaptive',
              'content': {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "type": "AdaptiveCard",
            "version": "1.0",
            "body": [
              {
                "type": "Container",
                "items": [
                  {
                    "type": "TextBlock",
                    "text": "## Device Information",//Laptop Details",
                    "weight": "bolder",
                    "size": "medium"
                  },
                  {
                    "type": "TextBlock",
                    "text": "  ",
                    "weight": "bolder",
                    "size": "medium"
                  },
              {
                "type": "Container",
                "items": [
                  
                  {
                    "type": "FactSet",
                    "facts": [
                        {
                        "title": "### Username:",
                        "value": name
                      },
                      {
                        "title": "### UserId:",
                        "value": adid
                      },
                      {
                        "title": "### Laptop model:",
                        "value": laptopmodel
                      },
                      {
                        "title": "### Serial No:",
                        "value": laptopno
                      },
                      {
                        "title": "### Warranty End Date:",
                        "value": warranty
                      },
                      {
                        "title": "### Email Id:",
                        "value": email
                      }
                    ]
                  }
                ]
              }
            ]
              }    
            ]    } 
             };
             return infocard;     
};    

var loc=function(address) {
  var map={'contentType': 'application/vnd.microsoft.card.adaptive',
                    'content': {
                	"$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                	"type": "AdaptiveCard",
                	"version": "1.0",
                	"body": [
                		{
                			"type": "ColumnSet",
                			"columns": [
                				{
                					"type": "Column",
                					"width": "99",
                					"items": [
                						
                						{
                							"type": "Image",
                                             "size": "auto",
                							"url": "https://digitalwing.azurewebsites.net/webchat/location.png"
                						},
                                        {
                                                  "type": "TextBlock",
                                                  "text": "Click on Image to opens maps",
                                                  "weight": "bolder",
                                                  "wrap": true
                                                }
                					],
                					"selectAction": {
                						"type": "Action.OpenUrl",
                						"title": "open maps",
                						"url":"https://www.google.com/maps/search/"+address+"/"
                                        //"url":"https://www.google.com/maps/search/laptop+service+center+near+me/"
                                        //"url": "https://binged.it/2qCscMD"
                					}
                				}
                			]
                		}
                	]
                    }
                };
                return map;
};
module.exports={
  feedbackcard,
  fun,
  loc
}
