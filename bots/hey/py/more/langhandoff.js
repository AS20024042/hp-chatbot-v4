"use strict";
var MsTranslator = require('mstranslator');
var Promise = require('promise');
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
// var JSON = require('json');
const mongoose_provider_1 = require("./mongoose-provider");
var request = require('request');
// Options for state of a conversation
// Customer talking to bot, waiting for next available agent or talking to an agent
var ConversationState;
(function (ConversationState) {
    ConversationState[ConversationState["Bot"] = 0] = "Bot";
    ConversationState[ConversationState["Waiting"] = 1] = "Waiting";
    ConversationState[ConversationState["Agent"] = 2] = "Agent";
    ConversationState[ConversationState["Done"] = 3] = "Done";
})(ConversationState = exports.ConversationState || (exports.ConversationState = {}));
;

//***************start of translator part

function translate_lang(textgiven,userLang)//to convert LUIS output to user's language
{  
    //console.log("inside translation")
    return new Promise((resolve, reject) => {
        var ltd = userLang;

        //ltd.trim();
        console.log("value of ltd", userLang);
        if(ltd=='"en"')
        {
            resolve(textgiven);
        }
        
        else{
            console.log("ltd value in else::::",ltd);
           langdetect = "http://api.microsofttranslator.com/V2/Ajax.svc/Translate" +
                        "?text=" + encodeURIComponent(textgiven) +"&from=en" +'&to='+ltd
            request.get({
            url : langdetect,
            category:'generalnn',
            headers:{
              'Ocp-Apim-Subscription-Key' : '48855a697c214a56af2041134f72795b',
            },
            time : true
          },
          function(err, response,body)
          {
              var x = response.body;
              
              s = x.trim();
              s = s.replace(/^"(.*)"$/, '$1');
              console.log("response is", s);
              console.log("Request time in ms for translation", response.elapsedTime);
              console.log('statusCode:', response.statusCode);
              resolve(s);
          })
            
        }
        
    })                   //return data1;
}

function translate_lang_prompts(textgiven) //to transalate strings into english for LUIS to understand
{
    return new Promise((resolve, reject) => {
        //ltd = s;
        //ltd.trim();
        
       
        
            langdetect = "http://api.microsofttranslator.com/V2/Ajax.svc/Translate" +
                     "?text=" + encodeURIComponent(textgiven)+"&from=fr" + "&to=en" 
          request.get({
            url : langdetect,
            category:'generalnn',
            headers:{
              'Ocp-Apim-Subscription-Key' : '48855a697c214a56af2041134f72795b',
            },
            time : true
          },
          function(err, response,body)
          {
              var x = response.body;
              s = x.trim();
              s =  s.replace(/^"(.*)"$/, '$1');    
              console.log("response is", s);
              console.log("Request time in ms for translation", response.elapsedTime);
              console.log('statusCode:', response.statusCode);
              resolve(s);
          });
       
        
        //return data1;
    })
}
//**********end of translator***********
class Handoff {
    // if customizing, pass in your own check for isAgent and your own versions of methods in defaultProvider
    
    constructor(bot, isAgent, provider = new mongoose_provider_1.MongooseProvider()) {
        this.bot = bot;
        this.isAgent = isAgent;
        this.provider = provider;
        this.connectCustomerToAgent = (by, agentAddress) => __awaiter(this, void 0, void 0, function* () {
            return yield this.provider.connectCustomerToAgent(by, agentAddress);
        });
        this.modifyStatus = (by, status) => __awaiter(this, void 0, void 0, function* () {
            return yield this.provider.modifyStatus(by, status);
        });
        this.connectCustomerToBot = (by) => __awaiter(this, void 0, void 0, function* () {
            return yield this.provider.connectCustomerToBot(by);
        });
        this.queueCustomerForAgent = (by) => __awaiter(this, void 0, void 0, function* () {
            return yield this.provider.queueCustomerForAgent(by);
        });
        this.addToTranscript = (by, message) => __awaiter(this, void 0, void 0, function* () {
            let from = by.agentConversationId ? 'Agent' : 'Customer';
            return yield this.provider.addToTranscript(by, message, from);
        });
        this.addToSummary = (by, message) => __awaiter(this, void 0, void 0, function* () {
           
            return yield this.provider.addToSummary(by,message);
        });
        this.getConversation = (by, customerAddress) => __awaiter(this, void 0, void 0, function* () {
            return yield this.provider.getConversation(by, customerAddress);
        });
        this.getCurrentConversations = () => __awaiter(this, void 0, void 0, function* () {
            return yield this.provider.getCurrentConversations();
        });
        this.getCurrentWaiting = () => __awaiter(this, void 0, void 0, function* () {
            return yield this.provider.getCurrentWaiting();
        });
        this.provider.init();
    }
    routingMiddleware() {
        return {
            botbuilder: (session, next) => {
                // Pass incoming messages to routing method
                if (session.message.type === 'message') {
                    this.routeMessage(session, next);
                }
                else {
                    // allow messages of non 'message' type through 
                    next();
                }
            },
            send: (event, next) => __awaiter(this, void 0, void 0, function* () {
                // Messages sent from the bot do not need to be routed
                // Not all messages from the bot are type message, we only want to record the actual messages  
                if (event.type === 'message' && !event.entities) {
                    this.transcribeMessageFromBot(event, next);
                }
                else {
                    //If not a message (text), just send to user without transcribing
                    next();
                }
            })
        };
    }
    routeMessage(session, next) {
        if (this.isAgent(session)) {
            this.routeAgentMessage(session);
        }
        else {
            this.routeCustomerMessage(session, next);
        }
    }
    routeAgentMessage(session) {
        return __awaiter(this, void 0, void 0, function* () {
            const message = session.message;// add translator here
            // session.conversationData.lang='fr';
            // console.log("User language in agent", session.conversationData.lang);
             
            const conversation = yield this.getConversation({ agentConversationId: message.address.conversation.id }, message.address);
              if(conversation.state==2){
				console.log("&&**##",message);
				// if( session.conversationData.lang=='fr'|| session.conversationData.lang=="'fr'"){
                console.log("User language in agent", conversation.summary.userlang);
                if(conversation.summary.userlang=='"fr"'||conversation.summary.userlang=="'fr'"){
                 console.log("inside if od agent",conversation.summary.userlang);
                translate_lang(message.text, 'fr').then(function (str1_ans) {
                        // session.sendTyping();
                        message.text=str1_ans;
                        console.log("&&#**##",message.text);
                         })
                }
            }
			
            yield this.addToTranscript({ agentConversationId: message.address.conversation.id }, message);
            // if the agent is not in conversation, no further routing is necessary
            if (!conversation)
                return;
            //if state of conversation is not 2, don't route agent message
            if (conversation.state !== ConversationState.Agent) {
                // error state -- should not happen
                session.send("Shouldn't be in this state - agent should have been cleared out.");
                return;
            }
            // send text that agent typed to the customer they are in conversation with
            this.bot.send(new builder.Message().address(conversation.customer).text(message.text).addEntity({ "agent": true }));
        });
    }
    routeCustomerMessage(session, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const message = session.message;
            const languser=session.conversationData.lang;
            
            // method will either return existing conversation or a newly created conversation if this is first time we've heard from customer
            const conversation = yield this.getConversation({ customerConversationId: message.address.conversation.id }, message.address);
            if(conversation.state==2){
				console.log("&&**##",message);
				if( session.conversationData.lang=='fr'|| session.conversationData.lang=="'fr'"){
                translate_lang_prompts(message.text, session.conversationData.lang).then(function (str1_ans) {
                        // session.sendTyping();
                        message.text=str1_ans;
                        console.log("&&#**##",message.text);
                         })
            }
			}
            yield this.addToTranscript({ customerConversationId: conversation.customer.conversation.id }, message);
            console.log('************ back', session.conversationData.summary);
            console.log("@@@@@@@@@", session.conversationData.lang);
            if(session.conversationData.endSession == "end"){
                console.log('********* ending session');
                
            }
            if(session.conversationData.summary!=undefined && session.conversationData.summary.utterance != undefined && session.conversationData.summary.intent != undefined){
                 console.log('************ after', session.conversationData.userInfo);
                const summary = session.conversationData.summary;
                yield this.addToSummary({ customerConversationId: conversation.customer.conversation.id }, summary);
            }
            switch (conversation.state) {
                
                //I WAS HERE
                case ConversationState.Done:
                    return next();
                case ConversationState.Bot:
                    return next();
                case ConversationState.Waiting:
                    translate_lang("Connecting you to the next available agent.", session.conversationData.lang).then(function (str1_ans) {
                        session.sendTyping();
                        session.send(str1_ans);})
                    return;
                case ConversationState.Agent:
                    if (!conversation.agent) {
                        session.send("No agent address present while customer in state Agent");
                        console.log("No agent address present while customer in state Agent");
                        return;
                    }
                    this.bot.send(new builder.Message().address(conversation.agent).text(message.text));
                    return;
            }
        });
    }
    // These methods are wrappers around provider which handles data
    transcribeMessageFromBot(message, next) {
        this.provider.addToTranscript({ customerConversationId: message.address.conversation.id }, message, 'Bot');
        next();
    }
    getCustomerTranscript(by, session) {
        return __awaiter(this, void 0, void 0, function* () {
            const customerConversation = yield this.getConversation(by);
            if (customerConversation) {
                var completeString= '';
                // customerConversation.transcript.forEach(transcriptLine => session.send(transcriptLine.text));
               // customerConversation.transcript.forEach(transcriptLine => (transcriptLine.from=='Bot') ? session.send('Bot'+ '-' +transcriptLine.text):session.send('Customer'+'-'+transcriptLine.text));
            customerConversation.transcript.forEach(transcriptLine => (transcriptLine.from=='Bot') ? completeString=completeString+('Bot'+ ': ' +transcriptLine.text+'\n'):completeString=completeString+('Customer'+': '+transcriptLine.text + '\n'));
                session.send(completeString);
            }
            else {
                session.send('No Transcript to show. Try entering a username or try again when connected to a customer');
            }
        });
    }

 getCustomerSummary(by, session) {
        return __awaiter(this, void 0, void 0, function* () {
            const customerConversation = yield this.getConversation(by);
            if (customerConversation) {
                // customerConversation.transcript.forEach(transcriptLine => session.send(transcriptLine.text));
             // console.log(customerConversation.summary.userUtterance,customerConversation.summary,customerConversation);
               session.send('User Issue:  '+customerConversation.summary.userUtterance +'\n'+'Identified issue category: ' + customerConversation.summary.botIntent);
               //session.send('Predicted Intent:  '+customerConversation.summary.botIntent);
            }
            else {
                session.send('No Summary to show. Try entering a username or try again when connected to a customer');
            }
        });
    }
}
exports.Handoff = Handoff;

//# sourceMappingURL=handoff.js.map